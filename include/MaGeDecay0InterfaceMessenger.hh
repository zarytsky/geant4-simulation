//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// ********************************************************************
// History:
// --------
// 25 Oct 2004   L.Pandola    First implementation

#ifndef MaGeDecay0InterfaceMessenger_H
#define MaGeDecay0InterfaceMessenger_H 1

class MaGeDecay0Interface;
class G4UIdirectory;
class G4UIcmdWithAString; 
class G4UIcommand;

#include "G4UImessenger.hh"
#include "globals.hh"

class MaGeDecay0InterfaceMessenger : public G4UImessenger
{
public:

  MaGeDecay0InterfaceMessenger(MaGeDecay0Interface*);
  ~MaGeDecay0InterfaceMessenger();
  void SetNewValue(G4UIcommand*,G4String);

private:
  MaGeDecay0Interface* fPointerToInterface;
  G4UIdirectory* fDirectory;
  G4UIcommand* fCommand;
};

#endif

