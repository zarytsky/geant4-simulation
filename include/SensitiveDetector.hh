#ifndef SensitiveDetector_h
#define SensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "RunAction.hh"
#include "G4RunManager.hh"
#include "StoreManager.hh"
#include "SdData.hpp"

class SensitiveDetector : public G4VSensitiveDetector 
{
protected:
    G4String _detName;

    SdStoreData* _sdData;

    G4int _ntupleGroupId;

public:
    SensitiveDetector(G4String detName, SdStoreData* sdData, G4int ntupleGroupId);

    ~SensitiveDetector();

    void Initialize(G4HCofThisEvent*);

    virtual void EndOfEvent(G4HCofThisEvent*);

    virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* touchableHistory);

    G4String GetDetName() const;

    const SdStoreData* GetSdData() const;

    G4int GetNtupleGroupId() const;
};

#endif
