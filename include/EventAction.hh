#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "RunAction.hh"
#include "G4RunManager.hh"

class EventAction : public G4UserEventAction
{
public:
	EventAction(RunAction* runAction);

	virtual ~EventAction();

	virtual void BeginOfEventAction(const G4Event* event);

	virtual void EndOfEventAction(const G4Event* event);

	RunAction* GetRunAction() { return _runAction; }

private:
	RunAction* _runAction;
};

#endif

		
