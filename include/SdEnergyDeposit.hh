#include "SdData.hpp"

#pragma once

class SdEnergyDeposit : public SdStoreData 
{
public:
    SdEnergyDeposit(G4String particleName);

    G4bool ReadData(G4Step*, G4TouchableHistory*) override;

private:
    G4String _particleName;
};