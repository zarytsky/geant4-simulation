#include "SdData.hpp"
#include "G4RunManager.hh"
#include "RunAction.hh"

#pragma once

class SdPointSurface: public SdStoreData 
{
public:  
    SdPointSurface(G4String volumeName);

    G4bool ReadData(G4Step*, G4TouchableHistory*) override;

private:
    RunAction* _runAction;
};
