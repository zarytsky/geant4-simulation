#ifndef RunActionMessenger_h
#define RunActionMessenger_h 1

#include "globals.hh"
#include "RunAction.hh"
#include "G4UImessenger.hh"
#include "G4UIcmdWithAnInteger.hh"

class RunAction;

class RunActionMessenger: public G4UImessenger
{
public:
    RunActionMessenger(RunAction*);
    
    ~RunActionMessenger();

    void SetNewValue(G4UIcommand *command, G4String newValues);
    
private:
    RunAction* _runAction;

	G4UIcmdWithAnInteger* _pgaStorePrimaryEnergyCommand;
};

#endif