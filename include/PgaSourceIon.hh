#ifndef PgaSourceIon_h
#define PgaSourceIon_h 1

#include "G4ParticleDefinition.hh"
#include "G4ParticleGun.hh"
#include "PgaSource.hh"

class PgaSourceIon : public PgaSource
{
public:
    PgaSourceIon(G4int a, G4int z, G4double energy, G4int j);

    ~PgaSourceIon();

    void BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator) override;

    void GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator) override;

    void SetIonA(G4int a);

    G4int GetIonA();

    void SetIonZ(G4int z);

    G4int GetIonZ();

private:
    G4int _ionA;

    G4int _ionZ;

    G4double _energy;

    G4int _ionJ;
};

#endif
