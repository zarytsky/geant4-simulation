#include "PgaPoint.hh"
#include <functional>
#include "Randomize.hh"
#include "G4Navigator.hh"

#pragma once

struct MaterialInCylinderParameters
{
    G4ThreeVector center;

    G4double heightMax;

    G4double radiusMax;

    G4double angleMin = 0 * radian;

    G4double angleMax = 2 * CLHEP::pi * radian;

    G4String materialName;
};

class PgaPointMaterialInCylinder : public PgaPoint
{
    MaterialInCylinderParameters _parameters;

    int _ratioEventsNumber;

    G4double _a;

    double GetVolumeRatio();

    G4double GetMaterialDensity();

public:
    PgaPointMaterialInCylinder(MaterialInCylinderParameters parameters);

    ~PgaPointMaterialInCylinder();

    G4ThreeVector GetPoint() override;

    void BeginOfRunAction() override;
};