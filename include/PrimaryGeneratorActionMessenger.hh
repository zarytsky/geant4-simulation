#ifndef PrimaryGeneratorActionMessenger_h
#define PrimaryGeneratorActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4ParticleTable.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4Tokenizer.hh"
#include "PgaSourceDecay0.hh"
#include "MaGeDecay0Interface.hh"
#include "PgaSourceIon.hh"
#include "PgaSourceTableFlat.hh"
#include "PgaPointFile.hh"
#include "PgaPointMaterial.hh"
#include "PgaPointSingle.hh"
#include "PgaPointVolume.hh"
#include "PgaPointSurface.hpp"
#include "PgaPointCylinder.hpp"
#include "PgaPointMaterialInCylinder.hpp"
#if HAS_BXDECAY0
#include "BxDecay0ParticleGun.hpp"
#include "PgaSourceBxDecay0.hpp"
#endif

class PrimaryGeneratorAction;

class PrimaryGeneratorActionMessenger: public G4UImessenger
{
public:
	PrimaryGeneratorActionMessenger(PrimaryGeneratorAction*);

	~PrimaryGeneratorActionMessenger();
	
    void SetNewValue(G4UIcommand *command, G4String newValues);

	void SetVolumeCandidates(G4String listVolumes);

	void SetMaterialCandidates(G4String listMaterials);
	
private:
	void InitDirectories();

	void InitSourceCommands();

	void InitPointCommands();

	G4String GetParticleNames();

	PrimaryGeneratorAction* _primaryGeneratorAction;

	G4ParticleTable* _particleTable;

	G4UIdirectory* _sourceDir;

	G4UIdirectory* _pointDir;

	// Source

	G4UIcommand* _sourceTableCommand;

	G4UIcommand* _sourceTableFlatCommand;

	G4UIcommand* _sourceDecay0Command;

	G4UIcommand* _sourceIonCommand;

	G4UIcommand* _sourceBxDecay0Command;

	// Point

	G4UIcmdWithAString* _pointFileCommand;

	G4UIcmdWithAString* _pointMaterialCommand;

	G4UIcmdWithAString* _pointVolumeCommand;

	G4UIcmdWith3Vector* _pointSingleCommand;

	G4UIcmdWithAString* _pointSurfaceCommand;

	G4UIcommand* _pointCylinderCommand;

	G4UIcommand* _pointMaterialInCylinderCommand;
};

#endif