#ifndef SteppingActionMessenger_h
#define SteppingActionMessenger_h 1

#include "SteppingAction.hpp"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4Tokenizer.hh"
#include "SdPointSurface.hh"
// #include "G4ParticleTable.hh"
// #include "SdEdepStandard.hpp"

class SteppingAction;

class SteppingActionMessenger: public G4UImessenger
{
public:
    SteppingActionMessenger(SteppingAction*);

    ~SteppingActionMessenger();

    void SetNewValue(G4UIcommand *command, G4String newValues);

    void SetVolumeCandidates(G4String volNames);

private:
    SteppingAction* _steppingAction;
    
    G4UIdirectory* _directory;

    G4UIcommand* _setPointSurface;

    G4UIcmdWithoutParameter* _volumesList;
};

#endif