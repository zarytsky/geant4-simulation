#include "PgaPoint.hh"
#include "G4Navigator.hh"
#include "G4VSolid.hh"
#include "Randomize.hh"
#include "G4Material.hh"
#include "G4TransportationManager.hh"
#include "G4PhysicalVolumeStore.hh"

#pragma once

class PgaPointVolume : public PgaPoint
{
public:
    PgaPointVolume(G4String volumeName);

    ~PgaPointVolume();

    G4ThreeVector GetPoint() override;

    void SetVolumeName(G4String volumeName);

    G4String GetVolumeName();

    void BeginOfRunAction() override;

private:
    void GetExtentSolidSizes();

    void SetVolume(G4VPhysicalVolume* volume);

    G4ThreeVector GetPointDistrLocal();

    G4ThreeVector GetPointDistrGlobal();

    G4VPhysicalVolume* GetMotherPhysicalVolume(G4VPhysicalVolume* volume);

    G4VSolid* _solid;

    G4String _volumeName;

    // diagonal points of box around volume where the random points to be generated
    G4ThreeVector _distrMin; 

    G4ThreeVector _distrMax;

    G4ThreeVector _distrR;

    const G4RotationMatrix* _distrRotation;

    G4ThreeVector _motherTranslation;

    G4RotationMatrix* _motherRotation;
};