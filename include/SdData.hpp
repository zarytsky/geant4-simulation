#include "DataValue.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "G4TouchableHistory.hh"

#pragma once

class SdStoreData 
{
protected:
    std::vector<IStorableData*> _dataValues;

public:
    SdStoreData();

    virtual ~SdStoreData();

    const std::vector<IStorableData*> GetDataValues() const;

    virtual G4bool ReadData(G4Step*, G4TouchableHistory*) = 0;
};