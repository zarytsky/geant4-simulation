#include "PgaPoint.hh"
#include "G4Navigator.hh"
#include "Randomize.hh"
#include "G4Material.hh"
#include "G4TransportationManager.hh"
#include "G4PhysicalVolumeStore.hh"

#pragma once

struct PointVolume;

class PgaPointMaterial : public PgaPoint
{
public:
    PgaPointMaterial(G4String matName);

    ~PgaPointMaterial();

    G4ThreeVector GetPoint() override;

    void SetMatName(G4String matName);

    G4String GetMatName();

    void BeginOfRunAction() override;

private:
    int GetRandomVolume();

    G4ThreeVector GetPointDistrLocal(PointVolume pointVolume);

    G4VPhysicalVolume* GetMotherPhysicalVolume(G4VPhysicalVolume* volume);

    void ApplyTransformation(PointVolume& pointVolume, G4VPhysicalVolume* volume);

    G4double _weightsSum;

    int _numberVolumes;

    std::vector<PointVolume> _pointVolumes;

    G4String _matName;
};

struct PointVolume
{
    G4double CubicVolume;

    G4VSolid* Solid;

    G4ThreeVector DistrMin;

    G4ThreeVector DistrMax;

    G4ThreeVector DistrR;

    G4RotationMatrix* Rotation;

    G4ThreeVector MotherTranslation;

    G4RotationMatrix* MotherRotation;
};