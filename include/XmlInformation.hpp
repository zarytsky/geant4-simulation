#include "globals.hh"
#include <vector>

#pragma once

struct XmlAttribute
{
    XmlAttribute() {}
    XmlAttribute(G4String name, G4String value) : Name(name), Value(value) { }
    G4String Name;
    G4String Value;
};

struct XmlTag 
{
    XmlTag() { }
    XmlTag(G4String name, G4String content) : Name(name), Content(content) { }
    G4String Name;
    G4String Content;
    std::vector<XmlAttribute*> Attributes;
};