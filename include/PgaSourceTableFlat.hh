#ifndef PgaSourceTableUniform_h
#define PgaSourceTableUniform_h 1

#include "PgaSource.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"

class PgaSourceTableFlat : public PgaSource
{
public:
    PgaSourceTableFlat(G4String name, G4double energyMin, G4double energyMax);

    void BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator) override;

    void GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator) override;

private:
    G4String _particleName;

    G4double _particleEnergyMin;

    G4double _particleEnergyMax;
};

#endif
