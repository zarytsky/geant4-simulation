#include "PgaPoint.hh"
#include "G4Navigator.hh"
#include "G4VSolid.hh"
#include "Randomize.hh"
#include "G4TransportationManager.hh"
#include "G4PhysicalVolumeStore.hh"

#pragma once

class PgaPointSurface : public PgaPoint
{
public:
    PgaPointSurface(G4String volumeName);

    ~PgaPointSurface();

    G4ThreeVector GetPoint() override;

    void SetVolumeName(G4String volumeName);

    G4String GetVolumeName();

    void BeginOfRunAction() override;

private:
    void SetVolume(G4VPhysicalVolume* volume);

    G4PhysicalVolumeStore* _volumeStore;

    // G4Navigator* _navigator;

    G4VSolid* _solid;

    G4String _volumeName;

    G4ThreeVector _distrR;
};