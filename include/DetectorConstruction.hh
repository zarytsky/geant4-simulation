#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "DetectorConstructionMessenger.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4SDManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4RunManager.hh"
#include "G4GDMLParser.hh"
#include "SensitiveDetector.hh"
#include "SdEnergyDeposit.hh"
#include "SteppingAction.hpp"
#include "G4Color.hh"
#include "RunAction.hh"

class DetectorConstructionMessenger;

class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
    DetectorConstruction();

    virtual ~DetectorConstruction();

    virtual G4VPhysicalVolume* Construct();

    virtual void ConstructSDandField();

    void SetDetectorByName(G4String detectorName, SdStoreData* sdData, G4int ntupleGroupId);

    void SetDetectorByVolume(G4LogicalVolume* logVolume, SdStoreData* sdData, G4int ntupleGroupId);

    void SetDetectorsByMaterial(G4String materialName, SdStoreData* sdData, G4int ntupleGroupId);

    void SetVolumeColor(G4String volumeName, G4Color color);

    void SetVolumesColorByMaterial(G4String materialName, G4Color color);

    G4String GetLogVolNames();

    G4String GetPhysVolNames();

    G4String GetMaterialNames();

    G4double GetMass(G4String volumeName);

    G4double GetVolume(G4String volumeName);

    G4double GetSurface(G4String volumeName);

protected:
    DetectorConstructionMessenger* _messenger;

    G4SDManager* _sdManager;

    G4GDMLParser* _gdmlParser;

    G4LogicalVolumeStore* _logVolueStore;

    G4PhysicalVolumeStore* _physVolumeStore;

    std::vector<G4LogicalVolume*> _logVolumes;
};

#endif

