#ifndef PgaSourceDecay0_h
#define PgaSourceDecay0_h 1

#include "G4ParticleDefinition.hh"
#include "G4ParticleGun.hh"
#include "PgaSource.hh"
#include "MaGeDecay0Interface.hh"
// #include "G4RandomDirection.hh"
// #include "G4RotationMatrix.hh"
// #include "G4ParticleTable.hh"

class PgaSourceDecay0 : public PgaSource
{
public:
    PgaSourceDecay0(G4String fileName);

    ~PgaSourceDecay0();

    void BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator) override;

    void GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator) override;

    void SetFileName(G4String fileName);

    G4String GetFileName();

private:
    G4String _fileName;
};

#endif
