#include "SdData.hpp"
#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4ProcessManager.hh"

#pragma once

class SdEdepProcess : public SdStoreData 
{
public:
    SdEdepProcess(G4String processName);

    G4bool ReadData(G4Step*, G4TouchableHistory*) override;

private:
    G4String _processName;
};