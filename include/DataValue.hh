#ifndef DataValue_h
#define DataValue_h 1

#include "globals.hh"
#include "G4VAnalysisManager.hh"

class IStorableData
{
protected:
    G4String _name;

    bool _isDefault;

public:
    virtual void Restore() = 0;

    virtual bool IsDefault() const = 0;

    virtual void Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const = 0;

    virtual G4int CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const = 0;

    G4String GetName() { return _name; }
};

template<typename T>
class DataValue : public IStorableData
{
protected:
    T _value;

    T _defaultValue;

public:
    DataValue(G4String name, T defaultValue);

    void SetValue(T value);

    void AddValue(T value);

    T GetValue() const;

    void Restore() override;

    bool IsDefault() const override;
};


class DataValueDouble : public DataValue<G4double>
{
public:
    DataValueDouble(G4String name, G4double defaultValue = 0);

    void Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const override;

    G4int CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const override;
};

class DataValueInt : public DataValue<G4int>
{
public:
    DataValueInt(G4String name, G4int defaultValue = 0);

    void Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const override;

    G4int CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const override;
};

class DataValueString : public DataValue<G4String>
{
public:
    DataValueString(G4String name, G4String defaultValue = "");

    void Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const override;

    G4int CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const override;
};

#endif