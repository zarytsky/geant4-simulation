#ifndef PgaSource_h
#define PgaSource_h 1

#include "G4ParticleGun.hh"
#include "G4SystemOfUnits.hh"
#include "G4RandomDirection.hh"
#include "G4VPrimaryGenerator.hh"
#include "XmlInformation.hpp"
#include "G4UIcommand.hh"

class PgaSource
{
protected:
    std::vector<XmlTag*> _informationTags;
    
public:
    PgaSource(){}

    ~PgaSource() {}

    virtual void BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator) = 0; 

    virtual void GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator) = 0;

    void SetTagValue(G4String tagName, G4String value)
    {
        for (auto &tag: _informationTags)
        {
            if (tag->Name == tagName)
            {
                tag->Content = value;
                break;
            }
        }
    }

    std::vector<XmlTag*> GetInformationTags() { return _informationTags; }
};

#endif
