#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4VPrimaryGenerator.hh"
#include "PrimaryGeneratorActionMessenger.hh"
#include "G4RandomDirection.hh"
#include "PgaPoint.hh"
#include "PgaSource.hh"
#include "PgaPointSingle.hh"
#include "PgaSourceTable.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "PgaStoreData.hpp"

class PrimaryGeneratorActionMessenger;


class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
    PrimaryGeneratorAction();

    virtual ~PrimaryGeneratorAction();

    virtual void GeneratePrimaries(G4Event*);   

    void BeginOfRunAction();

    void SetSource(PgaSource* pgaSource);

    void SetPoint(PgaPoint* pgaPoint);

    void SetParticleGun(G4VPrimaryGenerator* particleGun) { _primaryGenerator = particleGun; }

    const G4VPrimaryGenerator* GetParticleGun() const { return _primaryGenerator; }

    void SetMessengerVolumes(G4String names);

    void SetMessengerMaterials(G4String names);

    PgaSource* GetSource() { return _pgaSource; }

    PgaPoint* GetPoint() { return _pgaPoint; }

    std::vector<PgaStoreData*> StoreData;

private:
    G4VPrimaryGenerator* _primaryGenerator;

    PgaSource* _pgaSource;

    PgaPoint* _pgaPoint;

    PrimaryGeneratorActionMessenger* _messenger;

    G4String _classType;

};

#endif
