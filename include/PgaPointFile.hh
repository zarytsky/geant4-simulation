#include "PgaPoint.hh"
#include <fstream>

#pragma once

class PgaPointFile : public PgaPoint
{
public:
    PgaPointFile();

    PgaPointFile(G4String fileName, char separator = ',');

    ~PgaPointFile();

    G4ThreeVector GetPoint() override;

    void SetFileName(G4String fileName, char separator = ',');

    G4String GetFileName();

private:
    void OpenFile();

    G4String _fileName;

    char _separator;

    std::ifstream _inputStream;
};