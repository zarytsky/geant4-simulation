#ifndef StoreManager_h
#define StoreManager_h 1

#include "G4VAnalysisManager.hh"
#include "G4UIcommand.hh"
#include "SensitiveDetector.hh"
#include "SdData.hpp"
#include "G4ExceptionHandler.hh"
#include "StoreManagerMessenger.hpp"
#include <typeinfo>

class StoreManagerMessenger;
class SensitiveDetector;

struct NtupleStoreData
{
    G4String columnName;

    std::vector<G4int> columnIds;

    std::vector<IStorableData*> dataValues;
};

class StoreManager 
{
    const G4String DEFAULT_FILE_NAME = "Data";
    const G4String DEFAULT_NTUPLE_NAME = "Ntuple";
    const G4String FILE_NAME_INCREMENT = "_run";
    const G4String NON_ROOT_FILE_TYPE_NAME_POSTFIX_FORMAT = "_nt_{0}"; // where {0} is ntuple name

private:
    StoreManagerMessenger* _messenger;

    G4VAnalysisManager* _analysisManager;

    std::map<G4int, std::vector<NtupleStoreData*>> _ntupleStoreDataGroup;

    std::map<G4int, G4bool> _ntupleGroupStoreDefaults;

    std::map<G4int, G4int> _ntupleGroupToNtupleIdMapper;

    G4bool _recreate;

    void CreateNtuple(G4int ntupleId);

    void CreateColumns(G4int ntupleId, NtupleStoreData* storeDataModel);

    G4bool IsAllDataDefault(G4int ntupleId);

    void IncrementFileName();

    G4String GetFullFileName();

public:
    StoreManager(G4VAnalysisManager* analysisManager);

    ~StoreManager();

    void AddStoreData(G4int ntupleGroupId, G4String columnName, std::vector<IStorableData*> dataValues);

    void CreateStorage();

    void StoreData();

    void WriteDataToFile();


    void SetStoreDefaults(G4int ntupleGroupId, G4bool store);

    void SetRecreateFile(G4bool recreate);

    G4String GetFileName()
    {
        return _analysisManager->GetFileName();
    }

    G4String GetFileType()
    {
        return _analysisManager->GetFileType();
    }
};

#endif