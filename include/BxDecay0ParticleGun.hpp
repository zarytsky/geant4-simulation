#ifndef BxDecay0ParticleGun_h
#define BxDecay0ParticleGun_h 1

#include <fstream>
#include "globals.hh"
#include "G4VPrimaryGenerator.hh"
#include "G4PrimaryVertex.hh"
#include "G4SystemOfUnits.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"
#include "G4Alpha.hh"
#include "G4Event.hh"
#include "Randomize.hh"

#if HAS_BXDECAY0
#include "bxdecay0/decay0_generator.h"
#include "bxdecay0/std_random.h"
#include "bxdecay0/event.h"
#endif

class G4PrimaryVertex;

class BxDecay0ParticleGun : public G4VPrimaryGenerator
{
public:
    BxDecay0ParticleGun(G4String nuclideName);

    ~BxDecay0ParticleGun();

    void GeneratePrimaryVertex(G4Event* evt) override;

private:

#if HAS_BXDECAY0
    bxdecay0::decay0_generator* _decay0Generator;

    std::default_random_engine* _randomGenerator;
#endif

    G4String _nuclideName;
};

#endif