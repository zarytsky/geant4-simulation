#include "PgaPoint.hh"
#include <functional>
#include "Randomize.hh"

#pragma once

struct Cylinder
{
    G4ThreeVector center;

    G4double height;

    G4double radiusMin = 0;

    G4double radiusMax;

    G4double angleMin = 0 * radian;

    G4double angleMax = 2 * CLHEP::pi * radian;
};

class ICylinderPart
{
protected:
    Cylinder _cylinder;

public: 
    ICylinderPart(Cylinder cylinder) : _cylinder(cylinder)
    { }

    virtual G4ThreeVector GeneratePoint() = 0;

    virtual void Initalize() = 0;

    Cylinder GetCylinder() { return _cylinder; }

    void SetCylinder(Cylinder cylinder) { _cylinder = cylinder; }
};

class PartInside : public ICylinderPart
{
    G4double _a;

public:
    PartInside(Cylinder cylinder) : ICylinderPart(cylinder)
    { }

    G4ThreeVector GeneratePoint() override;

    void Initalize() override;
};

class PartSideSurface : public ICylinderPart
{
public:
    PartSideSurface(Cylinder cylinder) : ICylinderPart(cylinder)
    { }

    G4ThreeVector GeneratePoint() override;

    void Initalize() override;
};

struct SurfacePart
{    
    double weight;

    std::function<G4ThreeVector()> function;
};

class PartTotalSurface : public ICylinderPart
{
    G4double _a;

    std::vector<SurfacePart> _weights;

    G4double _weightsSum;

    int _numberParts;

    int GetRandomPart();

public:
    PartTotalSurface(Cylinder cylinder) : ICylinderPart(cylinder)
    { }

    G4ThreeVector GetPointInnerSide();

    G4ThreeVector GetPointOuterSide();

    G4ThreeVector GetPointTop();

    G4ThreeVector GetPointBottom();

    G4ThreeVector GeneratePoint() override;

    void Initalize() override;
};

class PgaPointCylinder : public PgaPoint
{
    ICylinderPart* _part;

public:
    PgaPointCylinder(ICylinderPart* part);

    ~PgaPointCylinder();

    G4ThreeVector GetPoint() override
    {
        return _part->GeneratePoint();
    }

    void BeginOfRunAction() override
    {
        _part->Initalize();
    }
};