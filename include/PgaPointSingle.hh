#include "PgaPoint.hh"

#pragma once

class PgaPointSingle : public PgaPoint
{
public:
    PgaPointSingle();

    PgaPointSingle(G4ThreeVector point);

    G4ThreeVector GetPoint() override;

    void SetPoint(G4ThreeVector point);

private:
    G4ThreeVector _point;
};