#ifndef LogFileWriter_h
#define LogFileWriter_h 1

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <iostream>
#include <string>
#include <sstream>
#include <ctime>

#include "RunAction.hh"
#include "G4UIcommand.hh"
#include "G4UImanager.hh"
#include "XmlInformation.hpp"
#include "G4LogicalVolumeStore.hh"
#include "SensitiveDetector.hh"
#include "G4Material.hh"
#include "globals.hh"

using namespace xercesc;
XERCES_CPP_NAMESPACE_USE

// #pragma once

class RunAction;

class LogFileWriter 
{
private:
    RunAction* _runAction;

    XMLCh* XmlStr(G4String text);

    G4String GetTimeDateString(time_t time);

    void Write(DOMDocument* document, G4String filePath);

    G4String GetGdmlFileName();

    DOMElement* CreateTag(DOMDocument* document, G4String tagName, std::vector<XmlTag*> infoTags);

    std::vector<G4LogicalVolume*> GetSensitiveVolumes();
    
    DOMElement* CreateDetectorsTag(DOMDocument* document);

public:
    LogFileWriter(RunAction* runAction);

    ~LogFileWriter();

    void CreateLogDocument(G4String filePath);
};

#endif
