#include "PgaStoreData.hpp"
#include "G4ParticleGun.hh"

#pragma once

class PgaStorePrimaryEnergy : public PgaStoreData 
{
public:
    PgaStorePrimaryEnergy();

    void ReadData(G4VPrimaryGenerator* primaryGenerator) override;
};