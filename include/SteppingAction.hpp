#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "SteppingActionMessenger.hh"
#include "EventAction.hh"
#include "RunAction.hh"
#include "SdData.hpp"
#include "globals.hh"

class SteppingActionMessenger;

class SteppingAction : public G4UserSteppingAction
{
public:
	SteppingAction(G4UserEventAction* eventAction);
		
	virtual ~SteppingAction();

	virtual void UserSteppingAction(const G4Step* step);

	void SetSdData(SdStoreData* sdData);

	void SetMessengerVolumes(G4String volNames);

private:
	G4UserEventAction*  _eventAction;

	SteppingActionMessenger* _messenger;

	SdStoreData* _sdData;
};

#endif