//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// ********************************************************************
// History:
// --------
// 25 Oct 2004   L.Pandola    First implementation


#ifndef MaGeDecay0Interface_h
#define  MaGeDecay0Interface_h 1

#include <fstream>
#include "globals.hh"
#include "G4VPrimaryGenerator.hh"

class G4PrimaryVertex;
class G4Event;
class MaGeDecay0InterfaceMessenger;

class MaGeDecay0Interface : public G4VPrimaryGenerator
{
public:
  MaGeDecay0Interface(G4String fileName, int particleType);
  ~MaGeDecay0Interface();
  //Constructor, fileName is the name of the file with path

  void GeneratePrimaryVertex(G4Event* evt);
  void OpenFile();
  void ChangeFileName(G4String newFile);
  void ChangeParticleType(int particleType);

private:
  G4String ConvertCodeToName(G4int code);
  G4String fFileName;
  std::ifstream fInputFile;
  MaGeDecay0InterfaceMessenger* fTheMessenger;
  int _particleType;

};
#endif
