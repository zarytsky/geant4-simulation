#ifndef StoreManagerMessenger_h
#define StoreManagerMessenger_h 1

#include "StoreManager.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4RootAnalysisManager.hh"
#include "G4CsvAnalysisManager.hh"
#include "G4XmlAnalysisManager.hh"
#include "G4Tokenizer.hh"

class StoreManager;

class StoreManagerMessenger : public G4UImessenger
{
public: 
    StoreManagerMessenger(StoreManager* storeManager);

    ~StoreManagerMessenger();

    void SetNewValue(G4UIcommand *command, G4String newValues);

private:
    StoreManager* _storeManager;

    G4UIdirectory* _storeDirectory;

    G4UIcmdWithAString* _storeTypeCommand;

    G4UIcmdWithABool* _recreateCommand;

    G4UIcommand* _storeDefaultsCommand;
};

#endif