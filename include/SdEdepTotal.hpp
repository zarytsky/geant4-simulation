#include "SdData.hpp"

#pragma once

class SdEdepTotal : public SdStoreData 
{
public:
    SdEdepTotal();

    G4bool ReadData(G4Step*, G4TouchableHistory*) override;
};