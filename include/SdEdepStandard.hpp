#include "SdData.hpp"

#pragma once

class SdEdepStandard : public SdStoreData 
{
    DataValueDouble* _energyGammaBeta;
    
    DataValueDouble* _energyAlpha;

public:
    SdEdepStandard();

    G4bool ReadData(G4Step*, G4TouchableHistory*) override;
};