#ifndef PgaSourceBxDecay0_h
#define PgaSourceBxDecay0_h 1

#include "PgaSource.hh"

class PgaSourceBxDecay0 : public PgaSource
{
public:
    PgaSourceBxDecay0(G4String nuclideName);

    ~PgaSourceBxDecay0();

    void BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator) override;

    void GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator) override;

private:
    G4String _nuclideName;
};

#endif
