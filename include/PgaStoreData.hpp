#include "DataValue.hh"
#include "G4VPrimaryGenerator.hh"

#pragma once

class PgaStoreData 
{
protected:
    std::vector<IStorableData*> _dataValues;

public:
    PgaStoreData();

    virtual ~PgaStoreData();

    const std::vector<IStorableData*> GetDataValues() const;

    virtual void ReadData(G4VPrimaryGenerator* primaryGenerator) = 0;
};