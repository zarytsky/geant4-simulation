#ifndef PgaSourceTable_h
#define PgaSourceTable_h 1

#include "PgaSource.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"

class PgaSourceTable : public PgaSource
{
public:
    PgaSourceTable(G4String name, G4double energy);

    void BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator) override;

    void GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator) override;

    void SetParticleName(G4String name);

    G4String GetParticleName();

    void SetParticleEnergy(G4double energy);

    G4double GetParticleEnergy();

private:
    G4String _particleName;

    G4double _particleEnergy;
};

#endif
