#ifndef RunAction_h
#define RunAction_h 1

#include "RunActionMessenger.hh"
#include "G4UserRunAction.hh"
#include "G4Run.hh"
#include "G4RootAnalysisManager.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4RunManager.hh"
#include "StoreManager.hh"
#include "SensitiveDetector.hh"
#include <chrono>
#include <ctime>
#include "LogFileWriter.hpp"
#include "PgaStoreData.hpp"

class RunActionMessenger;
class SensitiveDetector;
class StoreManager;

class RunAction: public G4UserRunAction
{
public:
	RunAction();

	~RunAction();

	void BeginOfRunAction(const G4Run*);

	void EndOfRunAction(const G4Run*);

	void AddDataToStore(SensitiveDetector* detector);

	void AddDataToStore(G4int ntupleGroupId, G4String storeTypeName, PgaStoreData* detector);

	void StoreAll();

	void CreateLogFile();

	StoreManager* GetStoreManager()
	{
		return _storeManager;
	}

	time_t GetStartTime()
	{
		return _timeStart;
	}

	time_t GetEndTime()
	{
		return _timeEnd;
	}

	G4int GetEventsGenerated()
	{
		return G4RunManager::GetRunManager()->GetNumberOfEventsToBeProcessed();
	}

	G4int GetEventsDetected()
	{
		return G4RunManager::GetRunManager()->GetNumberOfEventsToBeProcessed();
	}

	PrimaryGeneratorAction* GetPrimaryGeneratorAction()
	{
		return _pga;
	}

private:
	RunActionMessenger*_messenger;

	StoreManager* _storeManager;

	PrimaryGeneratorAction* _pga;

	time_t _timeStart;

	time_t _timeEnd;
};

#endif
