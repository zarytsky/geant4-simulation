#ifndef DetectorConstructionMessenger_h
#define DetectorConstructionMessenger_h 1

#include "DetectorConstruction.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4Tokenizer.hh"
#include "G4ParticleTable.hh"
#include "SdEdepStandard.hpp"
#include "G4Color.hh"
#include "G4UImanager.hh"
#include "SdEdepProcess.hpp"
#include "SdEdepTotal.hpp"
#include "G4ProcessManager.hh"
#include "G4LogicalVolumeStore.hh"

class DetectorConstruction;

class DetectorConstructionMessenger: public G4UImessenger
{
public:
    DetectorConstructionMessenger(DetectorConstruction*);

    ~DetectorConstructionMessenger();

    void SetNewValue(G4UIcommand *command, G4String newValues);

    void SetLogVolNames(G4String names);

    void SetMaterialNames(G4String names);

    G4String GetParticleNames();

    G4String GetProcessNames();

private:
    void InitEdepParticle();

    void InitEdepStandard();

    void InitEdepTotal();

    void InitGetProperties();

    void InitEdepParticleMaterial();

    void InitEdepStandardMaterial();

    void InitEdepTotalMaterial();

    void InitVolumeColor();

    void InitVolumeColorMaterial();

    void InitEdepProcess();

    DetectorConstruction* _detectorConstruction;

    G4UIcommand* _setEdepParticle;

    G4UIcommand* _setEdepStandard;

    G4UIcommand* _setEdepTotal;

    G4UIcommand* _setEdepParticleMaterial;

    G4UIcommand* _setEdepStandardMaterial;

    G4UIcommand* _setEdepTotalMaterial;

    G4UIcommand* _setEdepProcess;

    G4UIcmdWithoutParameter* _volumesList;

    G4UIcmdWithAString* _getMass;

    G4UIcmdWithAString* _getVolume;

    G4UIcmdWithAString* _getSurface;

    G4UIcommand* _setVolumeColor;

    G4UIcommand* _setVolumeColorMaterial;

    G4UImanager* _uiManager;
};

#endif