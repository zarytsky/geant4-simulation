#ifndef PgaPoint_h
#define PgaPoint_h 1

#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"
#include "XmlInformation.hpp"
#include "G4UIcommand.hh"

class PgaPoint
{
protected:
    std::vector<XmlTag*> _informationTags;

public:
    virtual G4ThreeVector GetPoint() = 0;

    virtual void BeginOfRunAction() {}

    std::vector<XmlTag*> GetInformationTags() { return _informationTags; }

    void SetTagValue(G4String tagName, G4String value)
    {
        std::for_each(_informationTags.begin(), _informationTags.end(), 
            [tagName, value] (XmlTag* tag)
            {
                if (tag->Name == tagName) tag->Content = value;
            });
    }
};

#endif
