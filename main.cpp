#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "ActionInitialization.hh"

#ifdef G4MULTITHREADED
    #include "G4MTRunManager.hh"
#else
    #include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "G4RunMessenger.hh"

#include "Randomize.hh"


int main(int argc, char** argv)
{
	std::vector<G4String> arguments(argv, argv + argc);
	// Detect interactive mode (if no arguments) and define UI session
	//
	G4UIExecutive* ui = 0;
	if ( argc == 1 || argc == 3 ) {
		ui = new G4UIExecutive(argc, argv);
	}

	// Choose the Random engine
	G4Random::setTheEngine(new CLHEP::RanecuEngine);
	G4Random::setTheSeed((G4long)time(NULL));
	
	// Construct the default run manager
    #ifdef G4MULTITHREADED
	    G4MTRunManager* runManager = new G4MTRunManager;
    #else
	    auto runManager = new G4RunManager;
    #endif

	runManager->SetUserInitialization(new DetectorConstruction);
	runManager->SetUserInitialization(new PhysicsList);
	runManager->SetUserInitialization(new ActionInitialization());
	// runManager->Initialize();

	G4VisManager* visManager = new G4VisExecutive;
	// G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
	// G4VisManager* visManager = new G4VisExecutive("Quiet");
	visManager->Initialize();

	// Get the pointer to the User Interface manager
	auto uiManager = G4UImanager::GetUIpointer();
	uiManager->ApplyCommand("/control/macroPath macros");

	G4String geometryName = "share/example/main.gdml";
	G4String geometryCommand = "-g";
	if (argc == 3 && arguments[1] == geometryCommand) geometryName = arguments[2];

	// Process macro or start UI session
	//
	if ( ! ui ) 
    { 
		// batch mode
		G4String command = "/control/execute ";
		G4String fileName = argv[1];
		uiManager->ApplyCommand(command+fileName);
	}
	else 
    { 
		// interactive mode
		uiManager->ApplyCommand("/control/verbose 2");
		uiManager->ApplyCommand("/run/verbose 2");
		uiManager->ApplyCommand("/persistency/gdml/read " + geometryName);

		uiManager->ApplyCommand("/run/initialize");

		uiManager->ApplyCommand("/vis/open OGL 600x600-0+0");
		uiManager->ApplyCommand("/vis/viewer/set/autoRefresh false");
		uiManager->ApplyCommand("/vis/verbose errors");
		uiManager->ApplyCommand("/vis/drawVolume");
		uiManager->ApplyCommand("/vis/viewer/set/viewpointVector -1 0 0");
		uiManager->ApplyCommand("/vis/viewer/set/lightsVector -1 0 0");
		uiManager->ApplyCommand("/vis/viewer/set/style wireframe");
		uiManager->ApplyCommand("/vis/viewer/set/auxiliaryEdge true");
		uiManager->ApplyCommand("/vis/viewer/set/lineSegmentsPerCircle 100");
		uiManager->ApplyCommand("/vis/scene/add/trajectories smooth");
		uiManager->ApplyCommand("/vis/modeling/trajectories/create/drawByCharge");
		uiManager->ApplyCommand("/vis/modeling/trajectories/drawByCharge-0/default/setDrawStepPts true");
		uiManager->ApplyCommand("/vis/modeling/trajectories/drawByCharge-0/default/setStepPtsSize 2");
		uiManager->ApplyCommand("/vis/scene/endOfEventAction accumulate");
		uiManager->ApplyCommand("/vis/set/textColour green");
		uiManager->ApplyCommand("/vis/set/textLayout right");
		uiManager->ApplyCommand("/vis/set/textLayout");
		uiManager->ApplyCommand("/vis/set/textColour");
		uiManager->ApplyCommand("/vis/viewer/set/style s");
		uiManager->ApplyCommand("/vis/viewer/set/hiddenMarker true");
		uiManager->ApplyCommand("/vis/viewer/set/viewpointThetaPhi 120 150");
		uiManager->ApplyCommand("/vis/viewer/set/autoRefresh true");
		uiManager->ApplyCommand("/vis/verbose warnings");
		uiManager->ApplyCommand("/vis/geometry/set/visibility World 0 false");

		ui->SessionStart();
		delete ui;
	}

	delete visManager;
	delete runManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
