# Geant4 simulation for the Double Beta Decay Experiments

Geant4 project aimed to be used as a base simulation for the Double Beta Decay Experiments purposes with common features implemented.

## Requirments:
- Ubuntu Linux (18.04/20.04 LTS)
- Geant4 of version 10.06.p02 or higher (not tested yet)
- Geant4 build with GDML support
- [Xerces](https://xerces.apache.org/) installed  
- *Optional* [BxDecay0](https://github.com/BxCppDev/bxdecay0) to use Decay0 without using files generated in advance

## Installation:
- Clone repository to your filesystem
- Make a build directory:
```bash
mkdir build
cd build
```
- Configure cmake with your Geant4 path:
```bash
cmake -DGeant4_DIR=/path/to/your/Geant4/install/lib/Geant4-**.*.* -DCMAKE_BUILD_TYPE=Release ../
```
- If you have a BxDecay0 installed use:
```bash
cmake -DGeant4_DIR=/path/to/your/Geant4/install/lib/Geant4-**.*.* -DBxDecay0_DIR=/path/to/your/BxDecay0/install/lib/cmake/BxDecay0 -DCMAKE_BUILD_TYPE=Release ../
```
- Make your build (using 4 cores):
```bash
make -j4
```

## Run example:
- In you build directory share/example folder will be copied
- Cd to this directory
```bash
cd share/example
```
- Run to open UI geometry viewer:
```bash
./../../simulation -g main.gdml
```
- Run macro file to perform a test simple simulation:
```bash
./../../simulation run.mac
```
- Macro file contains of the next commands:
```
/pga/source/setBxDecay0 Cs137 - generates Cs137 decay using BxDecay0 generator
/pga/point/setVolume Source_PV - set the volume Source_PV to be used as a source
/detector/setEdepTotal Detector - set volume Detector to detect the total energy deposit
/analysis/store/defaults 0 False - dont store empty (zero) events
```
- If BxDecay0 is not installed try replce the line /pga/source/setBxDecay0 with:
```
/pga/source/setTable gamma 2615 - generates gamma quantum with energy 2615 keV
```

## How to run:
- execute ./simulation -g geometry_file_name.gdml to run visualization
- execute ./simulation command.mac to run macros without visualiation

## Run options:
To run the simulation with the custom conditions of the particles vertices and kinematics one should use /pga/source/ and /pga/point/ commands:
- /pga/source/ defines the kinematics and type of particles to simulate
- /pga/point/ defines the verices and geometry where particles will be randomly simulated


