To use these scripts please install this package with pip 
Read over https://packaging.python.org/en/latest/tutorials/packaging-projects/

In processing/python_scripts directory run:
python3 -m build

Then install it with pip like:
pip3 install dist/dbcs_processing-*.tar.gz 