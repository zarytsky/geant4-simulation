from .helpers import find_element, get_crystal_masses, make_hist
import ROOT
from typing import Callable, List
from .constants import DEFAULT_HISTO_TITLE, DEFAULT_NTUPLE_NAME, EVENTS_GENERATED_LOG_TAG_NAME
from tqdm.notebook import tqdm

def get_total_spectrum_vertical(
    file_name: str, 
    detector_name_format: str, 
    detector_name_format_values: List[str],
    activity: float, 
    selector_format: str, 
    filter_function: Callable[[any, str], bool], 
    bins_number: int = 320, 
    energy_min: float = 0, 
    energy_max: float = 3200,
    log_file_name = "", 
    ntuple_name = DEFAULT_NTUPLE_NAME):

    data_file = ROOT.TFile(file_name, "READ")  # type: ignore
    tree = data_file.Get(ntuple_name)

    histigrams = dict()
    for format_value in detector_name_format_values:
        detector_name = detector_name_format.format(format_value)
        histigrams[detector_name] = ROOT.TH1F( # type: ignore
            detector_name, 
            DEFAULT_HISTO_TITLE,
            bins_number, 
            energy_min, 
            energy_max)

    for event in tqdm(tree, total = tree.GetEntriesFast()):
        for format_value in detector_name_format_values:
            if filter_function(event, format_value):
                detector_name = detector_name_format.format(format_value)
                histigrams[detector_name].Fill(getattr(event, selector_format.format(detector_name)))

    histogram_total = ROOT.TH1F(  # type: ignore
        "", 
        DEFAULT_HISTO_TITLE,
        bins_number, 
        energy_min, 
        energy_max)

    if log_file_name == "":
        log_file_name = file_name.rsplit('.', 1)[0] + '.log'
    
    CRYSTAL_MASSES = get_crystal_masses(log_file_name)
    bin_width = float(energy_max) / float(bins_number)
    events_generated = float(find_element(
        tag_name = EVENTS_GENERATED_LOG_TAG_NAME, 
        file_name = log_file_name).text)
    time = events_generated / activity / 31536000.0
    for format_value in detector_name_format_values:
        detector_name = detector_name_format.format(format_value)

        histigrams[detector_name].Scale(1.0 / bin_width / time / CRYSTAL_MASSES[detector_name])
        histigrams[detector_name].SetDirectory(0)
        
        histogram_total.Add(histigrams[detector_name])

    histogram_total = make_hist(histogram_total)
    histogram_total.SetDirectory(0)

    return histogram_total