from .get_spectrum import get_spectrum
import ROOT
from typing import Callable, List
from .constants import DEFAULT_HISTO_TITLE
from tqdm.notebook import tqdm

def get_total_spectrum(
    file_name: str, 
    detector_name_format: str, 
    detector_name_format_values: List[str],
    activity: float, 
    selector_format: str, 
    filter_function: Callable[[str, str], str], 
    bins_number: int = 320, 
    energy_min: float = 0, 
    energy_max: float = 3200):

    histogram_total = ROOT.TH1F(  # type: ignore
        "", 
        DEFAULT_HISTO_TITLE,
        bins_number, 
        energy_min, 
        energy_max)

    for format_value in tqdm(detector_name_format_values):
        detector_name = detector_name_format.format(format_value)
        selector = selector_format.format(detector_name)
        histogram = get_spectrum(
            file_name = file_name, 
            detector_name = detector_name, 
            selector = selector, 
            activity = activity, 
            filter = filter_function(detector_name_format, format_value), 
            bins_number = bins_number, 
            energy_min = energy_min, 
            energy_max = energy_max)

        histogram_total.Add(histogram)

    return histogram_total