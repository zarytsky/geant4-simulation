from .helpers import find_element, get_crystal_masses
from .constants import DEFAULT_NTUPLE_NAME, EVENTS_GENERATED_LOG_TAG_NAME
import numpy as np
import ROOT 
from typing import Callable, List

def get_BI_counts(
    energy_min: float, 
    energy_max: float, 
    file_name: str, 
    selector: str, 
    filter = "",
    ntuple_name = DEFAULT_NTUPLE_NAME):
    
    data_file = ROOT.TFile(file_name, "READ") # type: ignore
    tree = data_file.Get(ntuple_name)
    
    total_selection = "({0}) > {1} && ({0}) < {2}".format(selector, energy_min, energy_max)
    if (filter != ""):
        total_selection += " && ({0})".format(filter)
    counts = tree.GetEntries(total_selection)

    return counts

def get_total_BI(
    energy_min: float, 
    energy_max: float, 
    file_name: str, 
    detector_name_format: str, 
    detector_name_format_values: List[str], 
    activity: float, 
    selector_format: str, 
    filter_function: Callable[[str, str], str],
    log_file_name: str = ""):

    bi_counts_total: float = 0

    for format_value in detector_name_format_values:
        detector_name = detector_name_format.format(format_value)
        selector = selector_format.format(detector_name)
        bi_counts_total += get_BI_counts(energy_min, energy_max, 
            file_name = file_name, selector = selector, 
            filter = filter_function(detector_name_format, format_value))

    if log_file_name == "":
        log_file_name = file_name.rsplit('.', 1)[0] + '.log'

    crystal_masses = get_crystal_masses(log_file_name)
    events_generated = float(find_element(
        tag_name = EVENTS_GENERATED_LOG_TAG_NAME, 
        file_name = log_file_name).text)
    time = events_generated / activity / 31536000.0 # in years

    return {
        "value": bi_counts_total / time / crystal_masses[detector_name] / (energy_max - energy_min),
        "error": np.sqrt(bi_counts_total)  / time / crystal_masses[detector_name] / (energy_max - energy_min)
    }