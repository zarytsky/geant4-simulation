from .helpers import find_element, get_crystal_masses, make_hist
import ROOT
from .constants import DEFAULT_HISTO_TITLE, DEFAULT_NTUPLE_NAME, EVENTS_GENERATED_LOG_TAG_NAME

def get_spectrum(
    file_name: str, 
    detector_name: str, 
    selector: str, 
    activity: float, 
    log_file_name = "", 
    filter = "", 
    bins_number: int = 320, 
    energy_min: float = 0, 
    energy_max: float = 3200,
    ntuple_name = DEFAULT_NTUPLE_NAME):

    if log_file_name == "":
        log_file_name = file_name.rsplit('.', 1)[0] + '.log'
    
    CRYSTAL_MASSES = get_crystal_masses(log_file_name)
    
    data_file = ROOT.TFile(file_name, "READ")  # type: ignore
    tree = data_file.Get(ntuple_name)
    
    hist = ROOT.TH1F(  # type: ignore
        detector_name, 
        DEFAULT_HISTO_TITLE, 
        bins_number, 
        energy_min, 
        energy_max)
    
    tree.Draw("{0} >> {1}".format(selector, detector_name), filter)
    
    bin_width = float(energy_max) / float(bins_number)
    events_generated = float(find_element(
        tag_name = EVENTS_GENERATED_LOG_TAG_NAME, 
        file_name = log_file_name).text)
    time = events_generated / activity / 31536000.0
    
    hist.Scale(1.0 / bin_width / time / CRYSTAL_MASSES[detector_name])
    
    hist = make_hist(hist)
    
    hist.SetDirectory(0)

    return hist