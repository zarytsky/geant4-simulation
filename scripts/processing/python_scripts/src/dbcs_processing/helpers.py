import ROOT
import xml.etree.ElementTree as ET  

def make_hist(Hist2Change):
    HistNew = ROOT.TH1F(Hist2Change.GetName()+'_new',Hist2Change.GetTitle(), # type: ignore
    Hist2Change.GetNbinsX(),
    Hist2Change.GetXaxis().GetXmin(),
    Hist2Change.GetXaxis().GetXmax())
    for bin in range(Hist2Change.GetNbinsX()):
        HistNew.SetBinContent(bin,Hist2Change.GetBinContent(bin))
        HistNew.GetXaxis().SetTitle(Hist2Change.GetXaxis().GetTitle())
        HistNew.GetYaxis().SetTitle(Hist2Change.GetYaxis().GetTitle())
    return HistNew

def find_element(tag_name: str, file_name: str):
    doc1 = ET.parse(file_name)
    root = doc1.getroot()
    for element in root:
        if element.tag == tag_name:
            return element

def get_crystal_masses(file_name: str):
    results = dict()
    for element in find_element("detectors", file_name):
        results[element.attrib['name']] = float(element[2].text) / 1000.0
    return results