#include "PgaPointSurface.hpp"

PgaPointSurface::PgaPointSurface(G4String volumeName) : _volumeName(volumeName)
{
    _informationTags.push_back(new XmlTag("type", "Surface"));
    _informationTags.push_back(new XmlTag("name", _volumeName));
}

PgaPointSurface::~PgaPointSurface() { }

G4ThreeVector PgaPointSurface::GetPoint()
{
    auto point = _solid->GetPointOnSurface();
    point += _distrR;
    return point;
}

void PgaPointSurface::SetVolumeName(G4String volumeName)
{
    _volumeName = volumeName;
    SetTagValue("name", _volumeName);
}

G4String PgaPointSurface::GetVolumeName()
{
    return _volumeName;
}

void PgaPointSurface::BeginOfRunAction()
{
    auto volume = G4PhysicalVolumeStore::GetInstance()->GetVolume(_volumeName);
    if (volume == NULL)
        G4Exception("PgaPointVolume->BeginOfRunAction()",
            "Physical volume was not found.",
            FatalException, "");

    SetVolume(volume);

    auto surfaceArea = volume->GetLogicalVolume()->GetSolid()->GetSurfaceArea();
    auto surfaceAreaTag = new XmlTag("surfaceArea", G4UIcommand::ConvertToString(surfaceArea/cm2));
    surfaceAreaTag->Attributes.push_back(new XmlAttribute("unit", "cm2"));
    _informationTags.push_back(surfaceAreaTag);
}

void PgaPointSurface::SetVolume(G4VPhysicalVolume* volume)
{
    _solid = volume->GetLogicalVolume()->GetSolid();
    _distrR = volume->GetTranslation();
}

