#include "RunActionMessenger.hh"
#include "PgaStorePrimaryEnergy.hpp"

RunActionMessenger::RunActionMessenger(RunAction* runAction)
	: _runAction(runAction) 
{ 
	_pgaStorePrimaryEnergyCommand = new G4UIcmdWithAnInteger("/pga/store/primaryEnergy", this);
	_pgaStorePrimaryEnergyCommand->SetGuidance("Set ntupleGroupId (default 0) to store primary particle energy on each event");
	_pgaStorePrimaryEnergyCommand->SetGuidance("[usage] /pga/store/primaryEnergy ntupleGroupId");
	_pgaStorePrimaryEnergyCommand->SetDefaultValue(0);
	_pgaStorePrimaryEnergyCommand->SetParameterName("ntupleGroupId", true);
	_pgaStorePrimaryEnergyCommand->SetRange("ntupleGroupId >= 0");
}

RunActionMessenger::~RunActionMessenger()
{ 
	delete _pgaStorePrimaryEnergyCommand;
}

void RunActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
	if (command == _pgaStorePrimaryEnergyCommand)
	{
		G4Tokenizer next(newValue);
        auto ntupleGroupId = StoI(next());
		_runAction->AddDataToStore(ntupleGroupId, "primary", new PgaStorePrimaryEnergy());
	}
}
