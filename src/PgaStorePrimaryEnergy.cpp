#include "PgaStorePrimaryEnergy.hpp"

PgaStorePrimaryEnergy::PgaStorePrimaryEnergy()
{
    _dataValues.push_back(new DataValueDouble("energy"));
}

void PgaStorePrimaryEnergy::ReadData(G4VPrimaryGenerator* primaryGenerator)
{
    G4double energy = ((G4ParticleGun*)primaryGenerator)->GetParticleEnergy(); 
    ((DataValueDouble*)_dataValues.front())->SetValue(energy / CLHEP::keV);
}
