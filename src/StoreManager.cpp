#include "StoreManager.hh"

StoreManager::StoreManager(G4VAnalysisManager* analysisManager)
{
    _analysisManager = analysisManager;
    _analysisManager->SetFileName(DEFAULT_FILE_NAME);
    _messenger = new StoreManagerMessenger(this);
    _recreate = true;
}

StoreManager::~StoreManager()
{
    delete _messenger;
}

void StoreManager::AddStoreData(G4int ntupleGroupId, G4String columnName, std::vector<IStorableData*> dataValues)
{
    auto ntupleStoreDataModel = new NtupleStoreData();
    ntupleStoreDataModel->columnName = columnName;
    ntupleStoreDataModel->dataValues = dataValues;
    _ntupleStoreDataGroup[ntupleGroupId].push_back(ntupleStoreDataModel);
}

void StoreManager::CreateStorage()
{
    for (auto &ntupleStoreDataGroup : _ntupleStoreDataGroup)
    {
        auto ntupleGroupId = ntupleStoreDataGroup.first;
        auto name = DEFAULT_NTUPLE_NAME + G4UIcommand::ConvertToString(ntupleGroupId);
        auto ntupleId = _analysisManager->CreateNtuple(name, name);
        _ntupleGroupToNtupleIdMapper[ntupleGroupId] = ntupleId;

        for (auto &ntupleStoreData : ntupleStoreDataGroup.second)
            CreateColumns(ntupleId, ntupleStoreData);

        _analysisManager->FinishNtuple(ntupleId);
    }

    if (!_recreate) IncrementFileName();
  	_analysisManager->OpenFile();
}

void StoreManager::CreateColumns(G4int ntupleId, NtupleStoreData* storeDataModel)
{
    auto dataValues = storeDataModel->dataValues;
    for (unsigned int i = 0; i < dataValues.size(); i++)    
    {
        auto dataValue = dataValues.at(i);
        auto columnId = dataValue->CreateColumn(_analysisManager, ntupleId, 
            storeDataModel->columnName + "_" + dataValue->GetName());

        storeDataModel->columnIds.push_back(columnId);
    }
}

void StoreManager::StoreData()
{
    for (auto &ntupleStoreDataGroup : _ntupleStoreDataGroup)
    {
        auto ntupleGroupId = ntupleStoreDataGroup.first;
        auto ntupleId = _ntupleGroupToNtupleIdMapper[ntupleGroupId];
        if (!_ntupleGroupStoreDefaults[ntupleGroupId] && IsAllDataDefault(ntupleGroupId)) return;
    
        for (auto &storeData: ntupleStoreDataGroup.second)
        {
            auto dataValues = storeData->dataValues;
            auto columnIds = storeData->columnIds;
            for (unsigned int i = 0; i < dataValues.size(); i++)    
                dataValues.at(i)->Store(_analysisManager, ntupleId, columnIds[i]);
        }

        _analysisManager->AddNtupleRow(ntupleId);
    }
}

G4bool StoreManager::IsAllDataDefault(G4int ntupleGroupId)
{
    for (auto &dataDetector: _ntupleStoreDataGroup[ntupleGroupId])
    {
        for (auto &dataValue: dataDetector->dataValues)
        {
            if (!dataValue->IsDefault()) 
                return false;
        }
    }
    return true;
}

G4String StoreManager::GetFullFileName()
{
    return _analysisManager->GetFileName() + "." + _analysisManager->GetFileType();
}


void StoreManager::IncrementFileName()
{
    auto fileType = _analysisManager->GetFileType();
    G4String fileName;
    std::ifstream fileOut;

    int index = 0;
    do
    {
        auto postfix = index == 0 ? "" : FILE_NAME_INCREMENT + G4UIcommand::ConvertToString(index);
        fileName = _analysisManager->GetFileName() + postfix;

        fileOut.close();
        fileOut.open(fileName + "." + fileType);

        index++;
    } while(fileOut.good());

    _analysisManager->SetFileName(fileName);
}

void StoreManager::WriteDataToFile()
{
    _analysisManager->Write();
    _analysisManager->CloseFile();
}

void StoreManager::SetStoreDefaults(G4int ntupleGroupId, G4bool store)
{
    _ntupleGroupStoreDefaults[ntupleGroupId] = store;
}

void StoreManager::SetRecreateFile(G4bool recreate)
{
    _recreate = recreate;
}
