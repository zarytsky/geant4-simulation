#include "PgaPointSingle.hh"

// PgaPointSingle::PgaPointSingle()
// {
// }

PgaPointSingle::PgaPointSingle(G4ThreeVector point) : _point(point)
{
    _informationTags.push_back(new XmlTag("type", "Single"));
    _informationTags.push_back(new XmlTag("point", G4UIcommand::ConvertToString(_point)));
}

G4ThreeVector PgaPointSingle::GetPoint()
{
    return _point;
}

void PgaPointSingle::SetPoint(G4ThreeVector point)
{
    _point = point;
    SetTagValue("material", G4UIcommand::ConvertToString(_point));
}