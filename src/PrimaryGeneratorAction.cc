#include "PrimaryGeneratorAction.hh"
#include "PgaSourceTable.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
	: G4VUserPrimaryGeneratorAction()
{
	_primaryGenerator = new G4ParticleGun(1);
	_messenger = new PrimaryGeneratorActionMessenger(this);

	SetSource(new PgaSourceTable("geantino", 1));
	SetPoint(new PgaPointSingle(G4ThreeVector()));
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() 
{
	// delete _primaryGenerator;
	delete _messenger;
}

void PrimaryGeneratorAction::SetSource(PgaSource* pgaSource)
{
	_pgaSource = pgaSource;
}

void PrimaryGeneratorAction::SetPoint(PgaPoint* pgaPoint)
{
	_pgaPoint = pgaPoint;
}

void PrimaryGeneratorAction::BeginOfRunAction()
{
	_pgaPoint->BeginOfRunAction();	
	_pgaSource->BeginOfRunAction(_primaryGenerator);
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* event) 
{
	_primaryGenerator->SetParticlePosition(_pgaPoint->GetPoint());
	_pgaSource->GeneratePrimariesAction(_primaryGenerator);

	_primaryGenerator->GeneratePrimaryVertex(event);

	for (auto &storeData : StoreData)
		storeData->ReadData(_primaryGenerator);
}

void PrimaryGeneratorAction::SetMessengerVolumes(G4String names)
{
	_messenger->SetVolumeCandidates(names);
}

void PrimaryGeneratorAction::SetMessengerMaterials(G4String names)
{
	_messenger->SetMaterialCandidates(names);
}

