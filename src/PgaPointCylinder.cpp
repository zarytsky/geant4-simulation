#include "PgaPointCylinder.hpp"

PgaPointCylinder::PgaPointCylinder(ICylinderPart* part) : _part(part)
{
    auto cylinder = part->GetCylinder();
    _informationTags.push_back(new XmlTag("type", "Cylinder"));
    _informationTags.push_back(new XmlTag("center", G4UIcommand::ConvertToString(cylinder.center)));
    _informationTags.push_back(new XmlTag("height", G4UIcommand::ConvertToString(cylinder.height)));
    _informationTags.push_back(new XmlTag("radiusMax", G4UIcommand::ConvertToString(cylinder.radiusMax)));
    _informationTags.push_back(new XmlTag("radiusMin", G4UIcommand::ConvertToString(cylinder.radiusMin)));
    _informationTags.push_back(new XmlTag("angleMax", G4UIcommand::ConvertToString(cylinder.angleMax)));
    _informationTags.push_back(new XmlTag("angleMin", G4UIcommand::ConvertToString(cylinder.angleMin)));
    _informationTags.push_back(new XmlTag("whereGenerated", typeid(*part).name()));
}

PgaPointCylinder::~PgaPointCylinder()
{
    // delete _part;
}

void PartInside::Initalize()
{
    _a = 2.0/(_cylinder.radiusMax * _cylinder.radiusMax - _cylinder.radiusMin * _cylinder.radiusMin);
}

G4ThreeVector PartInside::GeneratePoint()
{
    auto theta = G4RandFlat::shoot(_cylinder.angleMin / radian, _cylinder.angleMax / radian);
    auto r = sqrt(_cylinder.radiusMin / mm * _cylinder.radiusMin / mm + 2.0 * G4UniformRand() / (_a / mm));
    // auto h = G4UniformRand() * _cylinder.height / mm;
    auto h = G4RandFlat::shoot(-0.5 * _cylinder.height / mm, 0.5 * _cylinder.height / mm);

    G4ThreeVector pointLocal;
    pointLocal.setRhoPhiZ(r, theta, h);

    return _cylinder.center + pointLocal;
}

void PartSideSurface::Initalize()
{
}

G4ThreeVector PartSideSurface::GeneratePoint()
{
    auto theta = G4RandFlat::shoot(_cylinder.angleMin / radian, _cylinder.angleMax / radian);
    auto r = _cylinder.radiusMax / mm;
    auto h = G4RandFlat::shoot(-0.5 * _cylinder.height / mm, 0.5 * _cylinder.height / mm);

    G4ThreeVector pointLocal;
    pointLocal.setX(r * cos(theta));
    pointLocal.setY(r * sin(theta));
    pointLocal.setZ(h);

    return _cylinder.center + pointLocal;
}

G4ThreeVector PartTotalSurface::GetPointInnerSide()
{
    auto theta = G4RandFlat::shoot(_cylinder.angleMin / radian, _cylinder.angleMax / radian);
    auto r = _cylinder.radiusMin / mm;
    auto h = G4RandFlat::shoot(-0.5 * _cylinder.height / mm, 0.5 * _cylinder.height / mm);

    G4ThreeVector pointLocal;
    pointLocal.setX(r * cos(theta));
    pointLocal.setY(r * sin(theta));
    pointLocal.setZ(h);

    return _cylinder.center + pointLocal;
}

G4ThreeVector PartTotalSurface::GetPointOuterSide()
{
    auto theta = G4RandFlat::shoot(_cylinder.angleMin / radian, _cylinder.angleMax / radian);
    auto r = _cylinder.radiusMax / mm;
    auto h = G4RandFlat::shoot(-0.5 * _cylinder.height / mm, 0.5 * _cylinder.height / mm);

    G4ThreeVector pointLocal;
    pointLocal.setX(r * cos(theta));
    pointLocal.setY(r * sin(theta));
    pointLocal.setZ(h);

    return _cylinder.center + pointLocal;
}

G4ThreeVector PartTotalSurface::GetPointTop()
{
    auto theta = G4RandFlat::shoot(_cylinder.angleMin / radian, _cylinder.angleMax / radian);
    auto r = sqrt(_cylinder.radiusMin / mm * _cylinder.radiusMin / mm + 2.0 * G4UniformRand() / (_a / mm));
    auto h = 0.5 * _cylinder.height / mm;

    G4ThreeVector pointLocal;
    pointLocal.setRhoPhiZ(r, theta, h);

    return _cylinder.center + pointLocal;
}

G4ThreeVector PartTotalSurface::GetPointBottom()
{
    auto theta = G4RandFlat::shoot(_cylinder.angleMin / radian, _cylinder.angleMax / radian);
    auto r = sqrt(_cylinder.radiusMin / mm * _cylinder.radiusMin / mm + 2.0 * G4UniformRand() / (_a / mm));
    auto h = -0.5 * _cylinder.height / mm;

    G4ThreeVector pointLocal;
    pointLocal.setRhoPhiZ(r, theta, h);

    return _cylinder.center + pointLocal;
}

void PartTotalSurface::Initalize()
{
    auto rm2 = _cylinder.radiusMax * _cylinder.radiusMax - _cylinder.radiusMin * _cylinder.radiusMin;
    _a = 2.0/rm2;
    _weightsSum = 0;
    auto deltaTheta = _cylinder.angleMax / radian - _cylinder.angleMin / radian;

    SurfacePart outerSide;
    outerSide.weight =  deltaTheta * _cylinder.radiusMax / mm * _cylinder.height / mm;
    outerSide.function = std::bind(&PartTotalSurface::GetPointOuterSide, this);
    _weightsSum += outerSide.weight;
    _weights.push_back(outerSide);

    SurfacePart innerSide;
    innerSide.weight = deltaTheta * _cylinder.radiusMin / mm * _cylinder.height / mm;
    innerSide.function = std::bind(&PartTotalSurface::GetPointInnerSide, this);
    _weightsSum += innerSide.weight;
    _weights.push_back(innerSide);

    SurfacePart bottomSide;
    bottomSide.weight = rm2 * deltaTheta / 2.0;
    bottomSide.function = std::bind(&PartTotalSurface::GetPointBottom, this);
    _weightsSum += bottomSide.weight;
    _weights.push_back(bottomSide);

    SurfacePart topSide;
    topSide.weight = rm2 * deltaTheta / 2.0;
    topSide.function = std::bind(&PartTotalSurface::GetPointTop, this);
    _weightsSum += topSide.weight;
    _weights.push_back(topSide);

    _numberParts = _weights.size();
}

int PartTotalSurface::GetRandomPart()
{
    auto randomValue = G4RandFlat::shoot(_weightsSum);
    for (int i = 0; i < _numberParts; i++)
    {
        auto weight = _weights[i].weight;
        if (randomValue < weight)
            return i;
        randomValue -= weight;
    }
    return 0; // should not be reached
}

G4ThreeVector PartTotalSurface::GeneratePoint()
{
    return _weights.at(GetRandomPart()).function();
}



