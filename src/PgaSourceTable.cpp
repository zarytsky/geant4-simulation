#include "PgaSourceTable.hh"

PgaSourceTable::PgaSourceTable(G4String name, G4double energy) 
{
    _particleName = name;
    _particleEnergy = energy;

    _informationTags.push_back(new XmlTag("type", "Table"));
    _informationTags.push_back(new XmlTag("particle", _particleName));
    auto tagEnergy = new XmlTag("energy", G4UIcommand::ConvertToString(_particleEnergy/keV));
    tagEnergy->Attributes.push_back(new XmlAttribute("unit", "keV"));
    _informationTags.push_back(tagEnergy);
}

void PgaSourceTable::BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator)
{
    if (G4ParticleGun* particleGun = dynamic_cast<G4ParticleGun*>(primaryGenerator))
    {
        auto particleTable = G4ParticleTable::GetParticleTable();
        particleGun->SetParticleDefinition(particleTable->FindParticle(_particleName));
        particleGun->SetParticleEnergy(_particleEnergy); 
    }
}

void PgaSourceTable::GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator)
{
    if (G4ParticleGun* particleGun = dynamic_cast<G4ParticleGun*>(primaryGenerator))
    {
        particleGun->SetParticleMomentumDirection(G4RandomDirection());
    }
}

void PgaSourceTable::SetParticleName(G4String name)
{
    _particleName = name;
    SetTagValue("particle", (_particleName));
}

G4String PgaSourceTable::GetParticleName()
{
    return _particleName;
}

void PgaSourceTable::SetParticleEnergy(G4double energy)
{
    _particleEnergy = energy;
    SetTagValue("energy", G4UIcommand::ConvertToString(_particleEnergy));
}

G4double PgaSourceTable::GetParticleEnergy()
{
    return _particleEnergy;
}