#include "PgaSourceIon.hh"
#include "G4SystemOfUnits.hh"
#include "G4IonTable.hh"
#include "G4UIcommand.hh"

PgaSourceIon::PgaSourceIon(G4int a, G4int z, G4double energy, G4int j) 
    : _ionA(a), _ionZ(z), _energy(energy), _ionJ(j)
{
    _informationTags.push_back(new XmlTag("type", "Ion"));
    _informationTags.push_back(new XmlTag("a", G4UIcommand::ConvertToString(_ionA)));
    _informationTags.push_back(new XmlTag("z", G4UIcommand::ConvertToString(_ionZ)));
    auto tagEnergy = new XmlTag("energy", G4UIcommand::ConvertToString(_energy/keV));
    tagEnergy->Attributes.push_back(new XmlAttribute("unit", "keV"));
    _informationTags.push_back(tagEnergy);
    _informationTags.push_back(new XmlTag("j", G4UIcommand::ConvertToString(_ionJ)));
}

void PgaSourceIon::BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator)
{
    if (G4ParticleGun* particleGun = dynamic_cast<G4ParticleGun*>(primaryGenerator))
    {
        particleGun->SetParticleDefinition(G4IonTable::GetIonTable()->GetIon(_ionZ, _ionA, _energy, _ionJ));
        particleGun->SetParticleCharge(0.*eplus);
    }
}

void PgaSourceIon::GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator)
{
    if (G4ParticleGun* particleGun = dynamic_cast<G4ParticleGun*>(primaryGenerator))
    {
        particleGun->SetParticleMomentumDirection(G4RandomDirection());
    }
}

void PgaSourceIon::SetIonA(G4int a)
{
    _ionA = a;

    SetTagValue("a", G4UIcommand::ConvertToString(_ionA));
}

G4int PgaSourceIon::GetIonA()
{
    return _ionA;
}

void PgaSourceIon::SetIonZ(G4int z)
{
    _ionZ = z;

    SetTagValue("z", G4UIcommand::ConvertToString(_ionZ));
}

G4int PgaSourceIon::GetIonZ()
{
    return _ionZ;
}