#include "SensitiveDetector.hh"

SensitiveDetector::SensitiveDetector(G4String detName, SdStoreData* sdData, G4int ntupleId)
    : G4VSensitiveDetector(detName), _detName(detName), _sdData(sdData), _ntupleGroupId(ntupleId)
{
}

SensitiveDetector::~SensitiveDetector() 
{
    delete _sdData;
}

void SensitiveDetector::Initialize(G4HCofThisEvent*)
{
    for (auto &dataValue : _sdData->GetDataValues())
        dataValue->Restore();
}

G4bool SensitiveDetector::ProcessHits(G4Step* step, G4TouchableHistory* touchableHistory)
{
    return _sdData->ReadData(step, touchableHistory);
}

void SensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
}

G4String SensitiveDetector::GetDetName() const
{
    return _detName;
}

const SdStoreData* SensitiveDetector::GetSdData() const
{
    return _sdData;
}

G4int SensitiveDetector::GetNtupleGroupId() const
{
    return _ntupleGroupId;
}


