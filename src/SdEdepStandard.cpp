#include "SdEdepStandard.hpp"

SdEdepStandard::SdEdepStandard()
{
    _energyGammaBeta = new DataValueDouble("energy_GammaBeta");
    _energyAlpha = new DataValueDouble("energy_Alpha");
    _dataValues.push_back(_energyGammaBeta);
    _dataValues.push_back(_energyAlpha);
}

G4bool SdEdepStandard::ReadData(G4Step* step, G4TouchableHistory*)
{
    auto particleName = step->GetTrack()->GetDefinition()->GetParticleName();

    if (particleName == "gamma" || particleName == "e-" || particleName == "e+")
        _energyGammaBeta->AddValue(step->GetTotalEnergyDeposit() / keV);
    else if (particleName == "alpha")
        _energyAlpha->AddValue(step->GetTotalEnergyDeposit() / keV);
    else
        return false;

    return true;
}