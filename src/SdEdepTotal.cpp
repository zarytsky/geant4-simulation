#include "SdEdepTotal.hpp"

SdEdepTotal::SdEdepTotal()
{
    _dataValues.push_back(new DataValueDouble("totalEdep"));
}

G4bool SdEdepTotal::ReadData(G4Step* step, G4TouchableHistory*)
{
    ((DataValueDouble*)_dataValues.front())->AddValue(step->GetTotalEnergyDeposit() / keV);
    return true;
}
