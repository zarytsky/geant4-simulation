#include "EventAction.hh"

EventAction::EventAction(RunAction* runAction)
	: G4UserEventAction(), _runAction(runAction)
{

} 

EventAction::~EventAction()
{

}

void EventAction::BeginOfEventAction(const G4Event*)
{    

}

void EventAction::EndOfEventAction(const G4Event*)
{    
	_runAction->StoreAll();
}
