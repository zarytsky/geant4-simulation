#include "PgaPointMaterial.hh"

PgaPointMaterial::PgaPointMaterial(G4String matName) : _matName(matName)
{
    _informationTags.push_back(new XmlTag("type", "Material"));
}

PgaPointMaterial::~PgaPointMaterial()
{
}

void PgaPointMaterial::BeginOfRunAction()
{
    _weightsSum = 0;
    G4double density;
    for (auto &physVolume: *G4PhysicalVolumeStore::GetInstance())
    {
        auto logVolume = physVolume->GetLogicalVolume();
        if (logVolume->GetMaterial()->GetName() != _matName)
            continue;

        density = logVolume->GetMaterial()->GetDensity();

        auto solid = logVolume->GetSolid();
        auto cubicVolume = solid->GetCubicVolume();
        _weightsSum += cubicVolume;

        PointVolume pointVolume;
        pointVolume.Solid = solid;
        pointVolume.CubicVolume = cubicVolume;
        pointVolume.DistrR = physVolume->GetTranslation();
        pointVolume.Rotation = physVolume->GetRotation();

        auto motherPhysicalVolume = GetMotherPhysicalVolume(physVolume);
        pointVolume.MotherTranslation = motherPhysicalVolume->GetTranslation();
        pointVolume.MotherRotation = motherPhysicalVolume->GetRotation();

        // ApplyTransformation(pointVolume, motherPhysicalVolume);

        solid->BoundingLimits(pointVolume.DistrMin, pointVolume.DistrMax);
        
        _pointVolumes.push_back(pointVolume);
    }
    _numberVolumes = _pointVolumes.size();

    auto material = new XmlTag("material", _matName);
    material->Attributes.push_back(new XmlAttribute("density", G4UIcommand::ConvertToString(density/(g/cm3))));
    material->Attributes.push_back(new XmlAttribute("unit", "g/cm3"));
    _informationTags.push_back(material);

    auto totalMassValue = (_weightsSum/cm3) * (density/(g/cm3));
    auto totalMass = new XmlTag("totalMass", G4UIcommand::ConvertToString(totalMassValue));
    totalMass->Attributes.push_back(new XmlAttribute("unit", "g"));
    _informationTags.push_back(totalMass);
}

int PgaPointMaterial::GetRandomVolume()
{
    auto randomValue = G4RandFlat::shoot(_weightsSum);
    for (int i = 0; i < _numberVolumes; i++)
    {
        auto cubicVolume = _pointVolumes[i].CubicVolume;
        if (randomValue < cubicVolume)
            return i;
        randomValue -= cubicVolume;
    }
    return 0; // should not be reached
}

G4ThreeVector PgaPointMaterial::GetPointDistrLocal(PointVolume pointVolume)
{
    auto pos = G4ThreeVector();
    auto distrMin = pointVolume.DistrMin;
    auto distrMax = pointVolume.DistrMax;

    pos.setX(G4RandFlat::shoot(distrMin.getX(), distrMax.getX()));
    pos.setY(G4RandFlat::shoot(distrMin.getY(), distrMax.getY()));
    pos.setZ(G4RandFlat::shoot(distrMin.getZ(), distrMax.getZ()));

    return pos;
}

G4ThreeVector PgaPointMaterial::GetPoint()
{
    G4ThreeVector point = G4ThreeVector();
    EInside inside = kOutside;
    auto pointVolume = _pointVolumes[GetRandomVolume()];

    do 
    {
        point = GetPointDistrLocal(pointVolume);
        inside = pointVolume.Solid->Inside(point);
    } 
    while (inside != kInside);

    if (pointVolume.Rotation != nullptr)
        point *= *pointVolume.Rotation;
    point += pointVolume.DistrR;

    if (pointVolume.MotherRotation != nullptr)
        point *= *pointVolume.MotherRotation;
    point += pointVolume.MotherTranslation;

    return point;
}

void PgaPointMaterial::SetMatName(G4String matName)
{
    _matName = matName;
    SetTagValue("material", _matName);
}

G4String PgaPointMaterial::GetMatName()
{
    return _matName;
}

G4VPhysicalVolume* PgaPointMaterial::GetMotherPhysicalVolume(G4VPhysicalVolume* volume)
{
    auto motherLogicalVolume = volume->GetMotherLogical();
    for (auto &physVolume: *G4PhysicalVolumeStore::GetInstance())
    {
        if (physVolume->GetLogicalVolume() == motherLogicalVolume)
            return physVolume;
    }

    return nullptr;
}

void PgaPointMaterial::ApplyTransformation(PointVolume& pointVolume, G4VPhysicalVolume* volume)
{
    if (volume != nullptr)
    {
        pointVolume.DistrR += volume->GetTranslation();
        auto motherRotation = volume->GetRotation();
        if (pointVolume.Rotation == nullptr) pointVolume.Rotation = motherRotation;
        else pointVolume.Rotation = &pointVolume.Rotation->transform(*motherRotation);
    }
}
