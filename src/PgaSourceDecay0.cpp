#include "PgaSourceDecay0.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"

PgaSourceDecay0::PgaSourceDecay0(G4String fileName) 
    : _fileName(fileName)
{
    _informationTags.push_back(new XmlTag("type", "Decay0"));
    _informationTags.push_back(new XmlTag("fileName", _fileName));
}

PgaSourceDecay0::~PgaSourceDecay0()
{
}

void PgaSourceDecay0::BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator)
{
}

void PgaSourceDecay0::GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator)
{
}

void PgaSourceDecay0::SetFileName(G4String fileName)
{
    _fileName = fileName;

    SetTagValue("fileName", _fileName);
}

G4String PgaSourceDecay0::GetFileName()
{
    return _fileName;
}
