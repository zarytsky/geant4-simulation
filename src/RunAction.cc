#include "RunAction.hh"

RunAction::RunAction() 
{
	_messenger = new RunActionMessenger(this);
	_storeManager = new StoreManager(G4RootAnalysisManager::Instance());
	_pga = (PrimaryGeneratorAction*)G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction();
}

RunAction::~RunAction() 
{
	delete _messenger;
}

void RunAction::AddDataToStore(SensitiveDetector* detector)
{
	_storeManager->AddStoreData(detector->GetNtupleGroupId(), detector->GetDetName(), detector->GetSdData()->GetDataValues());
}

void RunAction::AddDataToStore(G4int ntupleGroupId, G4String storeTypeName, PgaStoreData* storeData)
{
	_storeManager->AddStoreData(ntupleGroupId, storeTypeName, storeData->GetDataValues());
	_pga->StoreData.push_back(storeData);
}

void RunAction::StoreAll()
{
	_storeManager->StoreData();
}

void RunAction::BeginOfRunAction(const G4Run*) 
{
	_pga->BeginOfRunAction();

	_storeManager->CreateStorage();

	auto runManager = G4RunManager::GetRunManager();
	runManager->SetPrintProgress(0.1*runManager->GetNumberOfEventsToBeProcessed());  

    _timeStart = std::time(0);
}

void RunAction::EndOfRunAction(const G4Run*) 
{
	_timeEnd = std::time(0);

	CreateLogFile();

	_storeManager->WriteDataToFile();
}

void RunAction::CreateLogFile()
{
	auto lofFileWriter = new LogFileWriter(this);
	auto fileName = _storeManager->GetFileName() + ".log";
	lofFileWriter->CreateLogDocument(fileName);
}


