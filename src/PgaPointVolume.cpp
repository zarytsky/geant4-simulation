#include "PgaPointVolume.hh"

PgaPointVolume::PgaPointVolume(G4String volumeName) : _volumeName(volumeName)
{
    _informationTags.push_back(new XmlTag("type", "Volume"));
    _informationTags.push_back(new XmlTag("name", _volumeName));
}

PgaPointVolume::~PgaPointVolume()
{
}

void PgaPointVolume::SetVolume(G4VPhysicalVolume* volume)
{
    _solid = volume->GetLogicalVolume()->GetSolid();
    _solid->BoundingLimits(_distrMin, _distrMax);
    _distrR = volume->GetTranslation();
    _distrRotation = volume->GetRotation();

    auto motherPhysicalVolume = GetMotherPhysicalVolume(volume);
    _motherTranslation = motherPhysicalVolume->GetTranslation();
    _motherRotation = motherPhysicalVolume->GetRotation();
}

G4VPhysicalVolume* PgaPointVolume::GetMotherPhysicalVolume(G4VPhysicalVolume* volume)
{
    auto motherLogicalVolume = volume->GetMotherLogical();
    for (auto &physVolume: *G4PhysicalVolumeStore::GetInstance())
    {
        if (physVolume->GetLogicalVolume() == motherLogicalVolume)
            return physVolume;
    }

    return nullptr;
}

void PgaPointVolume::BeginOfRunAction()
{
    auto volume = G4PhysicalVolumeStore::GetInstance()->GetVolume(_volumeName);
    if (volume == nullptr)
        G4Exception("PgaPointVolume->BeginOfRunAction()",
            "Physical volume was not found.",
            FatalException, "");

    SetVolume(volume);

    auto logVolume = volume->GetLogicalVolume();
    auto material = logVolume->GetMaterial();

    auto mass = logVolume->GetMass();
    auto massTag = new XmlTag("mass", G4UIcommand::ConvertToString(mass/g));
    massTag->Attributes.push_back(new XmlAttribute("unit", "g"));
    _informationTags.push_back(massTag);

    auto materialTag = new XmlTag("material", material->GetName());
    materialTag->Attributes.push_back(new XmlAttribute("density", G4UIcommand::ConvertToString(material->GetDensity()/(g/cm3))));
    materialTag->Attributes.push_back(new XmlAttribute("unit", "g/cm3"));
    _informationTags.push_back(materialTag);
}

G4ThreeVector PgaPointVolume::GetPointDistrLocal()
{
    auto pos = G4ThreeVector();

    pos.setX(G4RandFlat::shoot(_distrMin.getX(), _distrMax.getX()));
    pos.setY(G4RandFlat::shoot(_distrMin.getY(), _distrMax.getY()));
    pos.setZ(G4RandFlat::shoot(_distrMin.getZ(), _distrMax.getZ()));

    return pos;
}

G4ThreeVector PgaPointVolume::GetPointDistrGlobal()
{
    return GetPointDistrLocal() + _distrR;
}

G4ThreeVector PgaPointVolume::GetPoint()
{    
    G4ThreeVector point = G4ThreeVector();
    EInside inside = kOutside;

    do 
    {
        point = GetPointDistrLocal();
        inside = _solid->Inside(point);
    } 
    while (inside != kInside);

    point += _distrR;

    if (_distrRotation != nullptr)
        point *= *_distrRotation;

    if (_motherRotation != nullptr)
        point *= *_motherRotation;

    point += _motherTranslation;

    return point;
}

void PgaPointVolume::SetVolumeName(G4String volumeName)
{
    _volumeName = volumeName;
    SetTagValue("name", _volumeName);
}

G4String PgaPointVolume::GetVolumeName()
{
    return _volumeName;
}
