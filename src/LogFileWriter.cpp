#include "LogFileWriter.hpp"

LogFileWriter::LogFileWriter(RunAction* runAction) : _runAction(runAction)
{

}

LogFileWriter::~LogFileWriter()
{

}

XMLCh* LogFileWriter::XmlStr(G4String text)
{
    return XMLString::transcode(text);
}

G4String LogFileWriter::GetTimeDateString(time_t time)
{
    tm* timeStruct = std::localtime(&time);
    G4String result;
    result += G4UIcommand::ConvertToString(timeStruct->tm_mday) + ".";
    result += G4UIcommand::ConvertToString(timeStruct->tm_mon + 1) + ".";
    result += G4UIcommand::ConvertToString(timeStruct->tm_year + 1900) + " ";
    result += G4UIcommand::ConvertToString(timeStruct->tm_hour) + ":";
    result += G4UIcommand::ConvertToString(timeStruct->tm_min) + ":";
    result += G4UIcommand::ConvertToString(timeStruct->tm_sec);
    return result;
}

G4String LogFileWriter::GetGdmlFileName()
{
    G4String value;
    auto uiManager = G4UImanager::GetUIpointer();
    G4String commandName = "/persistency/gdml/read";
    auto n = -1;
    do
    {
        value = uiManager->GetPreviousCommand(++n);
        if (value.contains(commandName)) 
        {
            auto res = value.substr(commandName.size());
            return res;
        }
    }
    while (value != "");

    return "geometry/main.gdml";
}

DOMElement* LogFileWriter::CreateTag(DOMDocument* document, G4String tagName, std::vector<XmlTag*> infoTags)
{
    auto mainTag = document->createElement(XmlStr(tagName));
    for(auto &xmlTag: infoTags)
    {
        auto tag = document->createElement(XmlStr(xmlTag->Name));
        tag->appendChild(document->createTextNode(XmlStr(xmlTag->Content)));
        for (auto &xmlAttribute: xmlTag->Attributes)
            tag->setAttribute(XmlStr(xmlAttribute->Name), XmlStr(xmlAttribute->Value)); 
        mainTag->appendChild(tag);
    }
    return mainTag;
}

std::vector<G4LogicalVolume*> LogFileWriter::GetSensitiveVolumes()
{
    std::vector<G4LogicalVolume*> sensitiveVolumes;
    for (auto &logVolume: *G4LogicalVolumeStore::GetInstance())
	{
        if (logVolume->GetSensitiveDetector() != nullptr)
            sensitiveVolumes.push_back(logVolume);
	}
    return sensitiveVolumes;
}

DOMElement* LogFileWriter::CreateDetectorsTag(DOMDocument* document)
{
    auto sensitiveVolumes = GetSensitiveVolumes();
    auto mainTag = document->createElement(XmlStr("detectors"));
    for (auto &logVolume: sensitiveVolumes)
    {
        std::vector<XmlTag*> infoTags;
        auto sensitiveDetector = (SensitiveDetector*)logVolume->GetSensitiveDetector();
        infoTags.push_back(new XmlTag("type", typeid(*sensitiveDetector->GetSdData()).name()));

        auto material = logVolume->GetMaterial();
        auto materialTag = new XmlTag("material", material->GetName());
        auto density = material->GetDensity()/(g/cm3);
        materialTag->Attributes.push_back(new XmlAttribute("density", G4UIcommand::ConvertToString(density)));
        materialTag->Attributes.push_back(new XmlAttribute("unit", "g/cm3"));
        infoTags.push_back(materialTag);

        auto mass = logVolume->GetMass()/g;
        auto massTag = new XmlTag("mass", G4UIcommand::ConvertToString(mass));
        massTag->Attributes.push_back(new XmlAttribute("unit", "g"));
        infoTags.push_back(massTag);

        auto detectorTag = CreateTag(document, "detector", infoTags);
        detectorTag->setAttribute(XmlStr("name"), XmlStr(logVolume->GetName())); 

        mainTag->appendChild(detectorTag);
    }
    return mainTag;
}


void LogFileWriter::CreateLogDocument(G4String filePath)
{
    XMLPlatformUtils::Initialize();
 
    auto document = DOMImplementationRegistry::getDOMImplementation(XmlStr("core"))
        ->createDocument(XmlStr(""), XmlStr("simulation"), 0);
    auto pRootElement = document->getDocumentElement();
    
    pRootElement->appendChild(document->createComment(XmlStr("Please dont change the file.")));

    auto timeElement = document->createElement(XmlStr("time"));
    timeElement->setAttribute(XmlStr("dateTimeFormat"), XmlStr("dd.mm.yyyy hh:mm:ss")); 
    pRootElement->appendChild(timeElement);

    auto startTime = document->createElement(XmlStr("startTime"));
    auto startTimeT = _runAction->GetStartTime();
    startTime->appendChild(document->createTextNode(XmlStr(GetTimeDateString(startTimeT))));
    timeElement->appendChild(startTime);

    auto endTime = document->createElement(XmlStr("endTime"));
    auto endTimeT = _runAction->GetEndTime();
    endTime->appendChild(document->createTextNode(XmlStr(GetTimeDateString(endTimeT))));
    timeElement->appendChild(endTime);

    auto duration = document->createElement(XmlStr("duration"));
    duration->setAttribute(XmlStr("unit"), XmlStr("sec")); 
    G4int timeDuration = endTimeT - startTimeT;
    duration->appendChild(document->createTextNode(XmlStr(G4UIcommand::ConvertToString(timeDuration))));
    timeElement->appendChild(duration);

    auto eventsGenerated = document->createElement(XmlStr("eventsGenerated"));
    eventsGenerated->appendChild(document->createTextNode(XmlStr(G4UIcommand::ConvertToString(_runAction->GetEventsGenerated()))));
    pRootElement->appendChild(eventsGenerated);

    auto geometryFileName = document->createElement(XmlStr("gdmlFile"));
    geometryFileName->appendChild(document->createTextNode(XmlStr(GetGdmlFileName())));
    pRootElement->appendChild(geometryFileName);

    auto dataFile = document->createElement(XmlStr("dataFile"));
    auto storeManager = _runAction->GetStoreManager();
    auto fileName = storeManager->GetFileName() + "." + storeManager->GetFileType();
    dataFile->appendChild(document->createTextNode(XmlStr(fileName)));
    pRootElement->appendChild(dataFile);

    auto source = CreateTag(document, "source", _runAction->GetPrimaryGeneratorAction()->GetSource()->GetInformationTags());
    pRootElement->appendChild(source);

    auto point = CreateTag(document, "point", _runAction->GetPrimaryGeneratorAction()->GetPoint()->GetInformationTags());
    pRootElement->appendChild(point);

    pRootElement->appendChild(CreateDetectorsTag(document));

    Write(document, filePath);
 
    //Release The Document after the xml file has been written
    document->release();
    XMLPlatformUtils::Terminate();
}

void LogFileWriter::Write(DOMDocument* document, G4String filePath)
{   
    //Return the first registered implementation that has the desired features. In this case, we are after
    //a DOM implementation that has the LS feature... or Load/Save.
    auto pImplement = xercesc::DOMImplementationRegistry::getDOMImplementation(XmlStr("LS"));
    
    //From the DOMImplementation, create a DOMWriter.
    //DOMWriters are used to serialize a DOM tree [back] into an XML document.
    
    auto pSerializer = ((DOMImplementationLS*)pImplement)->createLSSerializer(); 
    DOMLSOutput *pOutput = ((DOMImplementationLS*)pImplement)->createLSOutput();
    DOMConfiguration *pConfiguration = pSerializer->getDomConfig();
    
    // Have a nice output
    if (pConfiguration->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true))
        pConfiguration->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true); 
    auto pTarget = new LocalFileFormatTarget(filePath);
    pOutput->setByteStream(pTarget);
    
    //pSerializer->write(document->getDocumentElement(), pOutput); // missing header "" if used
    pSerializer->write(document, pOutput); 
    
    delete pTarget;
    pOutput->release();
    pSerializer->release();
}
