#include "StoreManagerMessenger.hpp"

StoreManagerMessenger::StoreManagerMessenger(StoreManager* storeManager)
    : _storeManager(storeManager)
{
    _storeDirectory = new G4UIdirectory("/analysis/store/");
    _storeDirectory->SetGuidance("Store file management");

    // _storeTypeCommand = new G4UIcmdWithAString("/analysis/store/type", this);
    // _storeTypeCommand->SetGuidance("Set the store type.");
    // _storeTypeCommand->SetGuidance("Possible values: root, csv and xml");
    // _storeTypeCommand->SetParameterName("storeType", false);
    // _storeTypeCommand->SetCandidates("root csv xml");
    // _storeTypeCommand->SetDefaultValue("root");

    _recreateCommand = new G4UIcmdWithABool("/analysis/store/recreate", this);
    _recreateCommand->SetGuidance("Set the file recreation option");
    _recreateCommand->SetParameterName("recreate", false);
    _recreateCommand->SetDefaultValue(true);

    _storeDefaultsCommand = new G4UIcommand("/analysis/store/defaults", this);
    _storeDefaultsCommand->SetGuidance("Set the ntuple group ID to store an empty events.");
    _storeDefaultsCommand->SetGuidance("[usage] /analysis/store/defaults ntupleGroupId storeDefaults");

    auto parameterNtupleId = new G4UIparameter("ntupleGroupId", 'i', false);
    parameterNtupleId->SetGuidance("Ntuple Group ID");
    parameterNtupleId->SetParameterRange("ntupleGroupId >= 0");
    
    auto parameterStoreDefaults = new G4UIparameter("storeDefaults", 'b', false);
    parameterStoreDefaults->SetGuidance("To store the default (zero) events?");

    _storeDefaultsCommand->SetParameter(parameterNtupleId);
    _storeDefaultsCommand->SetParameter(parameterStoreDefaults);
}

StoreManagerMessenger::~StoreManagerMessenger()
{
    delete _storeDirectory;
    // delete _storeTypeCommand;
    delete _recreateCommand;
    delete _storeDefaultsCommand;
}

void StoreManagerMessenger::SetNewValue(G4UIcommand* command, G4String newValues)
{
    // if (command == _storeTypeCommand)
    // {
    //     if (newValues == "root")
    //         _storeManager->SetAnalysisManager(G4RootAnalysisManager::Instance());
    //     else if (newValues == "csv")
    //         _storeManager->SetAnalysisManager(G4CsvAnalysisManager::Instance());
    //     else if (newValues == "xml")
    //         _storeManager->SetAnalysisManager(G4XmlAnalysisManager::Instance());
    // }

    if (command == _recreateCommand)
    {
        _storeManager->SetRecreateFile(StoB(newValues));
    }

    if (command == _storeDefaultsCommand)
    {
        G4Tokenizer next(newValues);
        auto ntupleGroupId = StoI(next());
        auto storeDefault = StoB(next());
        _storeManager->SetStoreDefaults(ntupleGroupId, storeDefault);
    }
}
