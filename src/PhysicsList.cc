#include "PhysicsList.hh"
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4SystemOfUnits.hh"
#include "G4NuclideTable.hh"

PhysicsList::PhysicsList() : G4VModularPhysicsList()
{
	SetVerboseLevel(1);

	// Default physics
	RegisterPhysics(new G4DecayPhysics());

	// Radioactive decay
	RegisterPhysics(new G4RadioactiveDecayPhysics());

	// EM physics
	RegisterPhysics(new G4EmStandardPhysics());

	G4NuclideTable::GetInstance()->SetThresholdOfHalfLife(0.1*picosecond);
  	G4NuclideTable::GetInstance()->SetLevelTolerance(1.0 * keV);
}

PhysicsList::~PhysicsList()
{ 
}

void PhysicsList::SetCuts()
{
	G4VUserPhysicsList::SetCuts();
	// SetDefaultCutValue(10*m);
}  
