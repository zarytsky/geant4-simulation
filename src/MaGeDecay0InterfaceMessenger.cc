//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// ********************************************************************
// History:
// --------
// 25 Oct 2004   L.Pandola    First implementation

#include "MaGeDecay0Interface.hh"
#include "MaGeDecay0InterfaceMessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UImessenger.hh"
#include "G4Tokenizer.hh"

MaGeDecay0InterfaceMessenger::MaGeDecay0InterfaceMessenger(MaGeDecay0Interface* ptr) : 
  fPointerToInterface(ptr)
{
  fDirectory = new G4UIdirectory("/decay0/");
  fDirectory->SetGuidance("Control of Decay0 event generator");

  fCommand = new G4UIcommand("/decay0/filename",this);
  fCommand->SetGuidance("Set the file name for decay0 events");

  auto fileNameParameter = new G4UIparameter("fileName", 's', false);
	fileNameParameter->SetGuidance("TSet the file name for decay0 events");

	auto particleTypeParameter = new G4UIparameter("particleType", 'i', true);
	particleTypeParameter->SetGuidance("The only particle type. If 0 will be ignored");
	particleTypeParameter->SetGuidance("1 is for Gamma, 2 - for e+, 3 - for e-, 47 - for Alpha");
	particleTypeParameter->SetParameterCandidates("0 1 2 3 47");
  particleTypeParameter->SetDefaultValue(0);

  fCommand->SetParameter(fileNameParameter);
  fCommand->SetParameter(particleTypeParameter);
}

MaGeDecay0InterfaceMessenger::~MaGeDecay0InterfaceMessenger()
{
  delete fDirectory;
  delete fCommand;
}

void MaGeDecay0InterfaceMessenger::SetNewValue(G4UIcommand* cmd,G4String str)
{
  if (cmd == fCommand)
    {
		  G4Tokenizer next(str);
      fPointerToInterface->ChangeFileName(next());
      fPointerToInterface->ChangeParticleType(StoI(next()));
    }
}
