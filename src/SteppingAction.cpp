#include "SteppingAction.hpp"

SteppingAction::SteppingAction(G4UserEventAction* eventAction)
: G4UserSteppingAction(), _eventAction(eventAction)
{
    _messenger = new SteppingActionMessenger(this);
    // _sdData = nullptr;
}

SteppingAction::~SteppingAction()
{}

void SteppingAction::UserSteppingAction(const G4Step* step)
{
    // if (_sdData != nullptr) _sdData->ReadData(const_cast<G4Step*>(step), 0);
}

void SteppingAction::SetSdData(SdStoreData* sdData)
{
    // _sdData = sdData;
    // auto runAction = ((EventAction*)_eventAction)->GetRunAction();
    // runAction->AddData(_sdData);
    // runAction->GetStoreManager()->SetStoreDefaults(_sdData->NtupleId, false);
}

void SteppingAction::SetMessengerVolumes(G4String volNames)
{
    _messenger->SetVolumeCandidates(volNames);
}


