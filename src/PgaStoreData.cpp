#include "PgaStoreData.hpp"

PgaStoreData::PgaStoreData()
{
}

PgaStoreData::~PgaStoreData()
{
    _dataValues.clear();
}

const std::vector<IStorableData*> PgaStoreData::GetDataValues() const
{
    return _dataValues;
}