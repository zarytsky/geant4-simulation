#include "SteppingActionMessenger.hh"

SteppingActionMessenger::SteppingActionMessenger(SteppingAction* steppingAction)
    : _steppingAction(steppingAction)
{
    _directory = new G4UIdirectory("/stepping/");
    _directory->SetGuidance("Set stepping actions");

    _setPointSurface = new G4UIcommand("/stepping/setPointSurface", this);
	_setPointSurface->SetGuidance("Set the volume name which surface points will be stored");

	auto volName = new G4UIparameter("volumeName", 's', false);
	volName->SetGuidance("The volume name");

	auto ntupleId = new G4UIparameter("ntupleId", 'i', true);
	ntupleId->SetGuidance("ID of ntuple");
	ntupleId->SetDefaultValue(0);
	ntupleId->SetParameterRange("ntupleId >= 0");
	
	_setPointSurface->SetParameter(volName);
	_setPointSurface->SetParameter(ntupleId);
}

SteppingActionMessenger::~SteppingActionMessenger()
{
    delete _directory;
    delete _setPointSurface;
}

void SteppingActionMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{
    if (command == _setPointSurface)
	{
		// G4Tokenizer next(newValue);
        // auto volName = next();
        // auto ntupleId = StoI(next());
        // auto sdData = new SdPointSurface(volName);
        // sdData->NtupleId = ntupleId;
		// _steppingAction->SetSdData(sdData);
	}
}

void SteppingActionMessenger::SetVolumeCandidates(G4String volNames)
{
    _setPointSurface->GetParameter(0)->SetParameterCandidates(volNames);
}
