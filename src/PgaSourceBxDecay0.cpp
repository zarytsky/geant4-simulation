#include "PgaSourceBxDecay0.hpp"

PgaSourceBxDecay0::PgaSourceBxDecay0(G4String nuclideName) 
    : _nuclideName(nuclideName)
{
    _informationTags.push_back(new XmlTag("type", "BxDecay0"));
    _informationTags.push_back(new XmlTag("nuclideName", _nuclideName));
}

PgaSourceBxDecay0::~PgaSourceBxDecay0()
{
}

void PgaSourceBxDecay0::BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator)
{
}

void PgaSourceBxDecay0::GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator)
{
}

