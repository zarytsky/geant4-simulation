#include "SdEnergyDeposit.hh"

SdEnergyDeposit::SdEnergyDeposit(G4String particleName) 
    : _particleName(particleName)
{
    auto name = "energy_" + particleName + "_";
    std::replace(name.begin(), name.end(), '-', 'm');
    std::replace(name.begin(), name.end(), '+', 'p');

    _dataValues.push_back(new DataValueDouble(name));
}

G4bool SdEnergyDeposit::ReadData(G4Step* step, G4TouchableHistory*)
{
    if (step->GetTrack()->GetDefinition()->GetParticleName() == _particleName)
        ((DataValueDouble*)_dataValues.front())->AddValue(step->GetTotalEnergyDeposit() / keV);
    else
        return false;

    return true;
}