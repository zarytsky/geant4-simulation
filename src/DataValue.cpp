#include "DataValue.hh"

template<typename T>
DataValue<T>::DataValue(G4String name, T defaultValue) 
{ 
    _name = name;
    _defaultValue = defaultValue;
    this->Restore();
}

template<typename T>
void DataValue<T>::SetValue(T value)
{
    _value = value;
    if (_isDefault) _isDefault = false;
}

template<typename T>
void DataValue<T>::AddValue(T value)
{
    _value += value;
    if (_isDefault) _isDefault = false;
}

template<typename T>
T DataValue<T>::GetValue() const
{
    return _value;
}

template<typename T>
void DataValue<T>::Restore()
{
    _value = _defaultValue;
    _isDefault = true;
}

template<typename T>
bool DataValue<T>::IsDefault() const
{
    return _isDefault;
}

// DOUBLE

DataValueDouble::DataValueDouble(G4String name, G4double defaultValue)
    : DataValue<G4double>(name, defaultValue)
{ }

void DataValueDouble::Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const
{
    analysisManager->FillNtupleDColumn(ntupleId, columnId, _value);
}

G4int DataValueDouble::CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const
{
    return analysisManager->CreateNtupleDColumn(ntupleId, columnName);
}

// INT

DataValueInt::DataValueInt(G4String name, G4int defaultValue)
    : DataValue<G4int>(name, defaultValue)
{ }

void DataValueInt::Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const
{
    analysisManager->FillNtupleIColumn(ntupleId, columnId, _value);
}

G4int DataValueInt::CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const
{
    return analysisManager->CreateNtupleIColumn(ntupleId, columnName);
}

// STRING

DataValueString::DataValueString(G4String name, G4String defaultValue)
    : DataValue<G4String>(name, defaultValue)
{ }

void DataValueString::Store(G4VAnalysisManager* analysisManager, G4int ntupleId, G4int columnId) const
{
    analysisManager->FillNtupleSColumn(ntupleId, columnId, _value);
}

G4int DataValueString::CreateColumn(G4VAnalysisManager* analysisManager, G4int ntupleId, G4String columnName) const
{
    return analysisManager->CreateNtupleSColumn(ntupleId, columnName);
}

template class DataValue<double>; 
template class DataValue<int>; 
template class DataValue<G4String>; 