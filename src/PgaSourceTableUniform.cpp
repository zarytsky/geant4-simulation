#include "PgaSourceTableFlat.hh"

PgaSourceTableFlat::PgaSourceTableFlat(G4String name, G4double energyMin, G4double energyMax) 
{
    _particleName = name;
    _particleEnergyMin = energyMin;
    _particleEnergyMax = energyMax;

    _informationTags.push_back(new XmlTag("type", "TableUniform"));
    _informationTags.push_back(new XmlTag("particle", _particleName));

    auto tagEnergyMin = new XmlTag("energyMin", G4UIcommand::ConvertToString(_particleEnergyMin/keV));
    tagEnergyMin->Attributes.push_back(new XmlAttribute("unit", "keV"));
    _informationTags.push_back(tagEnergyMin);

    auto tagEnergyMax = new XmlTag("energyMax", G4UIcommand::ConvertToString(_particleEnergyMax/keV));
    tagEnergyMax->Attributes.push_back(new XmlAttribute("unit", "keV"));
    _informationTags.push_back(tagEnergyMax);
}

void PgaSourceTableFlat::BeginOfRunAction(G4VPrimaryGenerator* primaryGenerator)
{
    if (G4ParticleGun* particleGun = dynamic_cast<G4ParticleGun*>(primaryGenerator))
    {
        auto particleTable = G4ParticleTable::GetParticleTable();
        particleGun->SetParticleDefinition(particleTable->FindParticle(_particleName));
    }
}

void PgaSourceTableFlat::GeneratePrimariesAction(G4VPrimaryGenerator* primaryGenerator)
{
    if (G4ParticleGun* particleGun = dynamic_cast<G4ParticleGun*>(primaryGenerator))
    {
        particleGun->SetParticleMomentumDirection(G4RandomDirection());

        auto randomEnergy = G4RandFlat::shoot(_particleEnergyMin/keV, _particleEnergyMax/keV);
        particleGun->SetParticleEnergy(randomEnergy * keV); 
    }
}

