#include "PgaPointMaterialInCylinder.hpp"
#include "G4Navigator.hh"
#include "G4TransportationManager.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Material.hh"
#include "G4PhysicalVolumeStore.hh"

PgaPointMaterialInCylinder::PgaPointMaterialInCylinder(MaterialInCylinderParameters parameters) : _parameters(parameters)
{
    _informationTags.push_back(new XmlTag("type", "MaterialInCylinder"));
    _informationTags.push_back(new XmlTag("center", G4UIcommand::ConvertToString(parameters.center)));
    _informationTags.push_back(new XmlTag("heightMax", G4UIcommand::ConvertToString(parameters.heightMax)));
    _informationTags.push_back(new XmlTag("radiusMax", G4UIcommand::ConvertToString(parameters.radiusMax)));
    _informationTags.push_back(new XmlTag("angleMax", G4UIcommand::ConvertToString(parameters.angleMax)));
    _informationTags.push_back(new XmlTag("angleMin", G4UIcommand::ConvertToString(parameters.angleMin)));
    _informationTags.push_back(new XmlTag("materialName", parameters.materialName));

    _ratioEventsNumber = 1000000;
}

PgaPointMaterialInCylinder::~PgaPointMaterialInCylinder()
{

}

G4ThreeVector PgaPointMaterialInCylinder::GetPoint()
{
    auto gNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();

    G4ThreeVector resultPoint;

    do
    {
        auto theta = G4RandFlat::shoot(_parameters.angleMin / radian, _parameters.angleMax / radian);
        auto r = sqrt(2.0 * G4UniformRand() / (_a / mm));
        auto h = G4RandFlat::shoot(-0.5 * _parameters.heightMax / mm, 0.5 * _parameters.heightMax / mm);

        G4ThreeVector pointLocal;
        pointLocal.setRhoPhiZ(r, theta, h);

        resultPoint = _parameters.center + pointLocal;

    } while ( gNavigator->LocateGlobalPointAndSetup(resultPoint)->GetLogicalVolume()->GetMaterial()->GetName() != _parameters.materialName);

    return resultPoint;
}

void PgaPointMaterialInCylinder::BeginOfRunAction()
{
    _a = 2.0/(_parameters.radiusMax * _parameters.radiusMax);
    auto volumeRatio = GetVolumeRatio();
    G4double materialVolume = volumeRatio * CLHEP::pi * _parameters.radiusMax * _parameters.radiusMax * _parameters.heightMax * mm3;
    auto materialVolumeTag = new XmlTag("volume", G4UIcommand::ConvertToString(materialVolume / cm3));
    materialVolumeTag->Attributes.push_back(new XmlAttribute("unit", "cm3"));
    _informationTags.push_back(materialVolumeTag);

    auto density = GetMaterialDensity();
    auto materialMass = new XmlTag("mass", G4UIcommand::ConvertToString( (materialVolume / cm3) * density/(g/cm3)));
    materialMass->Attributes.push_back(new XmlAttribute("unit", "g"));
    _informationTags.push_back(materialMass);

    auto densityTag = new XmlTag("density", G4UIcommand::ConvertToString( density/(g/cm3)));
    densityTag->Attributes.push_back(new XmlAttribute("unit", "g/cm3"));
    _informationTags.push_back(densityTag);
}

double PgaPointMaterialInCylinder::GetVolumeRatio()
{
    auto gNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
    int countsInMaterial = 0;
    for (int i = 0; i < _ratioEventsNumber; i++)
    {
        auto theta = G4RandFlat::shoot(_parameters.angleMin / radian, _parameters.angleMax / radian);
        auto r = sqrt(2.0 * G4UniformRand() / (_a / mm));
        auto h = G4RandFlat::shoot(-0.5 * _parameters.heightMax / mm, 0.5 * _parameters.heightMax / mm);

        G4ThreeVector pointLocal;
        pointLocal.setRhoPhiZ(r, theta, h);

        auto resultPoint = _parameters.center + pointLocal;

        if (gNavigator->LocateGlobalPointAndSetup(resultPoint)->GetLogicalVolume()->GetMaterial()->GetName() == _parameters.materialName)
            countsInMaterial++;
    }
    return float(countsInMaterial) / float(_ratioEventsNumber);
}

G4double PgaPointMaterialInCylinder::GetMaterialDensity()
{
    for (auto &physVolume: *G4PhysicalVolumeStore::GetInstance())
    {
        auto material = physVolume->GetLogicalVolume()->GetMaterial();
        if (material->GetName() == _parameters.materialName)
            return material->GetDensity();
    }
    return 0;
}
    



