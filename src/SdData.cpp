#include "SdData.hpp"

SdStoreData::SdStoreData()
{
}

SdStoreData::~SdStoreData()
{
    _dataValues.clear();
}

const std::vector<IStorableData*> SdStoreData::GetDataValues() const
{
    return _dataValues;
}