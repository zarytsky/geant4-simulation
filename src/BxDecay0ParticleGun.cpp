#include "BxDecay0ParticleGun.hpp"

#if HAS_BXDECAY0

BxDecay0ParticleGun::BxDecay0ParticleGun(G4String nuclideName)
{
    particle_time = 0.0;

    _decay0Generator = new bxdecay0::decay0_generator;
    _decay0Generator->set_decay_category(bxdecay0::decay0_generator::DECAY_CATEGORY_BACKGROUND);
    _decay0Generator->set_decay_isotope(nuclideName);      

    _randomGenerator = new std::default_random_engine(G4Random::getTheSeed());    
    bxdecay0::std_random bxdRandomGenerator(*_randomGenerator); 
    _decay0Generator->initialize(bxdRandomGenerator);
}

BxDecay0ParticleGun::~BxDecay0ParticleGun()
{
    delete _decay0Generator;
}

void BxDecay0ParticleGun::GeneratePrimaryVertex(G4Event* event)
{
    bxdecay0::std_random bxdRandomGenerator(*_randomGenerator); 

    bxdecay0::event generatedDecay;
    _decay0Generator->shoot(bxdRandomGenerator, generatedDecay);

    double event_time = 0.0 * CLHEP::second;
    double particle_time = 0.0 * CLHEP::second; 

    auto vertex = new G4PrimaryVertex(particle_position, event_time);

    const auto & particles = generatedDecay.get_particles();
    for (const auto & particle : particles)
    {
        G4ParticleDefinition* particleDefinition;

        if (particle.is_electron()) {
            particleDefinition = G4Electron::ElectronDefinition();
        } else if (particle.is_positron()) {
            particleDefinition = G4Positron::PositronDefinition();
        } else if (particle.is_gamma()) {
            particleDefinition = G4Gamma::GammaDefinition();
        } else if (particle.is_alpha()) {
            particleDefinition = G4Alpha::AlphaDefinition();
        } else {
            throw std::logic_error("bxdecay9::PrimaryGeneratorAction::GeneratePrimaries: Unsupported particle type!");
        }

        if (particle.has_time()) {
            particle_time = particle.get_time() * CLHEP::second;
        } 
        particle_time += event_time;

        auto primaryParticle = new G4PrimaryParticle(particleDefinition, 
            particle.get_px() * CLHEP::MeV, particle.get_py() * CLHEP::MeV, particle.get_pz() * CLHEP::MeV);
        primaryParticle->SetProperTime(particle_time); 

        //Add particle to vertex
        vertex->SetPrimary(primaryParticle);
    }
    event->AddPrimaryVertex(vertex);
}

#endif