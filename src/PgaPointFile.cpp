#include "PgaPointFile.hh"

PgaPointFile::PgaPointFile()
{
}

PgaPointFile::PgaPointFile(G4String fileName, char separator) 
    : _fileName(fileName), _separator(separator)
{
    OpenFile();

    _informationTags.push_back(new XmlTag("type", "File"));
    _informationTags.push_back(new XmlTag("fileName", _fileName));
}

PgaPointFile::~PgaPointFile()
{
    _inputStream.close();
}

G4ThreeVector PgaPointFile::GetPoint()
{
    std::string line;
    do 
    {
        std::getline(_inputStream, line);
        if(_inputStream.eof())
        {
            _inputStream.clear();
            _inputStream.seekg(0, std::ios::beg);
        }

    } while (line.empty() || line[0] == '#');
    
    G4ThreeVector vector;
    std::string value;
    std::stringstream stream(line);
    std::getline(stream, value, ',');
    vector.setX(std::atof(value.c_str()));

    std::getline(stream, value, ',');
    vector.setY(std::atof(value.c_str()));

    std::getline(stream, value);
    vector.setZ(std::atof(value.c_str()));
    
    return vector;
}

void PgaPointFile::OpenFile()
{
    _inputStream.open(_fileName);   
    if (!_inputStream)
        G4Exception("PgaPointFile->OpenFile()","Cannot open file", FatalException, "");
}

void PgaPointFile::SetFileName(G4String fileName, char separator)
{
    _fileName = fileName;
    _separator = separator;
    OpenFile();
    SetTagValue("fileName", _fileName);
}

G4String PgaPointFile::GetFileName()
{
    return _fileName;
}


