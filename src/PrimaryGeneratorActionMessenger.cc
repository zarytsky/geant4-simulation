#include "PrimaryGeneratorActionMessenger.hh"
#include "PrimaryGeneratorAction.hh"

#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"

PrimaryGeneratorActionMessenger::PrimaryGeneratorActionMessenger(PrimaryGeneratorAction* primaryGeneratorAction)
	: _primaryGeneratorAction(primaryGeneratorAction) 
{ 
	_particleTable = G4ParticleTable::GetParticleTable();
	InitDirectories();
	InitSourceCommands();
	InitPointCommands();
}

PrimaryGeneratorActionMessenger::~PrimaryGeneratorActionMessenger()
{
	delete _sourceDir;
	delete _pointDir;
	delete _sourceTableCommand;
	delete _sourceDecay0Command;
	delete _sourceBxDecay0Command;
	delete _sourceIonCommand;
	delete _pointFileCommand;
	delete _pointMaterialCommand;
	delete _pointVolumeCommand;
	delete _pointSingleCommand;
	delete _pointCylinderCommand;
}

G4String PrimaryGeneratorActionMessenger::GetParticleNames()
{
	G4String candidateList;
	for(G4int i = 0; i < _particleTable->entries(); i++)
		candidateList += _particleTable->GetParticleName(i) + " ";
	return candidateList;
}

void PrimaryGeneratorActionMessenger::InitDirectories()
{
	_sourceDir = new G4UIdirectory("/pga/source/");
    _sourceDir->SetGuidance("The details of the particles to be generated");

	_pointDir = new G4UIdirectory("/pga/point/");
    _pointDir->SetGuidance("Place where the particles will be generated");
}

void PrimaryGeneratorActionMessenger::InitSourceCommands()
{
	// Particle Table

	_sourceTableCommand = new G4UIcommand("/pga/source/setTable", this);
	_sourceTableCommand->SetGuidance("Set the source to be a table particle with certain energy in keV");
	_sourceTableCommand->SetGuidance("[usage] /pga/source/setTable particleName energy(keV)");

	auto tableParticleName = new G4UIparameter("particleName", 's', false);
	tableParticleName->SetGuidance("The particle name");
	tableParticleName->SetDefaultValue("geantino");
	tableParticleName->SetParameterCandidates(GetParticleNames());

	auto tableParticleEnergy = new G4UIparameter("energy", 'd', false);
	tableParticleEnergy->SetGuidance("The particle energy in keV");
	tableParticleEnergy->SetParameterRange("energy >= 0");
	
	_sourceTableCommand->SetParameter(tableParticleName);
	_sourceTableCommand->SetParameter(tableParticleEnergy);

	// Particle Table Flat

	_sourceTableFlatCommand = new G4UIcommand("/pga/source/setTableFlat", this);
	_sourceTableFlatCommand->SetGuidance("Set the source to be a table particle with flat energy spectrum in range");
	_sourceTableFlatCommand->SetGuidance("[usage] /pga/source/setTable particleName minEnergy(keV) maxEnergy(keV)");

	auto tableFlatParticleMinEnergy = new G4UIparameter("minEnergy", 'd', false);
	tableFlatParticleMinEnergy->SetGuidance("Minimal particle energy in keV");
	tableFlatParticleMinEnergy->SetParameterRange("minEnergy >= 0");

	auto tableFlatParticleMaxEnergy = new G4UIparameter("maxEnergy", 'd', false);
	tableFlatParticleMaxEnergy->SetGuidance("Maximum particle energy in keV");
	tableFlatParticleMaxEnergy->SetParameterRange("maxEnergy >= 0");
	
	_sourceTableFlatCommand->SetParameter(tableParticleName);
	_sourceTableFlatCommand->SetParameter(tableFlatParticleMinEnergy);
	_sourceTableFlatCommand->SetParameter(tableFlatParticleMaxEnergy);

	// Decay0

	_sourceDecay0Command = new G4UIcommand("/pga/source/setDecay0", this);
	_sourceDecay0Command->SetGuidance("Set the file path of the Decay0 output");
	_sourceDecay0Command->SetGuidance("[usage] /pga/source/setDecay0 fileName particleType");

	auto fileNameParameter = new G4UIparameter("fileName", 's', false);
	fileNameParameter->SetGuidance("TSet the file name for decay0 events");

	auto particleTypeParameter = new G4UIparameter("particleType", 'i', true);
	particleTypeParameter->SetGuidance("The only particle type. If 0 will be ignored");
	particleTypeParameter->SetGuidance("1 is for Gamma, 2 - for e+, 3 - for e-, 47 - for Alpha");
	particleTypeParameter->SetParameterCandidates("0 1 2 3 47");
	particleTypeParameter->SetDefaultValue(0);

	_sourceDecay0Command->SetParameter(fileNameParameter);
	_sourceDecay0Command->SetParameter(particleTypeParameter);

	_sourceIonCommand = new G4UIcommand("/pga/source/setIon", this);
	_sourceIonCommand->SetGuidance("Set the ion A and Z");
	_sourceIonCommand->SetGuidance("[usage] /pga/source/setIon A Z");

	auto ionA = new G4UIparameter("A", 'i', false);
	ionA->SetGuidance("Ion A");
	ionA->SetParameterRange("A > 0");

	auto ionZ = new G4UIparameter("Z", 'i', false);
	ionZ->SetGuidance("Ion Z");
	ionZ->SetParameterRange("Z > 0");

	auto ionE = new G4UIparameter("energy", 'd', true);
	ionE->SetGuidance("Ion excitation energy");
	ionE->SetDefaultValue(0);
	ionE->SetParameterRange("energy >= 0");

	auto ionJ = new G4UIparameter("J", 'i', true);
	ionJ->SetGuidance("Ion J");
	ionJ->SetDefaultValue(0);
	ionJ->SetParameterRange("Z > 0");

	_sourceIonCommand->SetParameter(ionA);
	_sourceIonCommand->SetParameter(ionZ);
	_sourceIonCommand->SetParameter(ionE);
	_sourceIonCommand->SetParameter(ionJ);

	// BxDecay0
	#if HAS_BXDECAY0

	_sourceBxDecay0Command = new G4UIcommand("/pga/source/setBxDecay0", this);

	auto nuclideNameParameter = new G4UIparameter("nuclideName", 's', false);
	nuclideNameParameter->SetGuidance("Set nuclide name");

	_sourceBxDecay0Command->SetParameter(nuclideNameParameter);

	#endif
}

void PrimaryGeneratorActionMessenger::InitPointCommands()
{
	_pointFileCommand = new G4UIcmdWithAString("/pga/point/setFile", this);
	_pointFileCommand->SetGuidance("Set the file path to search for the point");
	_pointFileCommand->SetGuidance("[usage] /pga/point/setFile fileName");

	_pointMaterialCommand = new G4UIcmdWithAString("/pga/point/setMaterial", this);
	_pointMaterialCommand->SetGuidance("Set the material name");
	_pointMaterialCommand->SetGuidance("[usage] /pga/point/setMaterial materialName");

	_pointVolumeCommand = new G4UIcmdWithAString("/pga/point/setVolume", this);
	_pointVolumeCommand->SetGuidance("Set the volume name");
	_pointVolumeCommand->SetGuidance("[usage] /pga/point/setVolume volumeName");

	_pointSingleCommand = new G4UIcmdWith3Vector("/pga/point/setSingle", this);
	_pointSingleCommand->SetGuidance("Set the global point");
	_pointSingleCommand->SetGuidance("[usage] /pga/point/setSingle x y z");
	_pointSingleCommand->SetDefaultValue(G4ThreeVector());

	_pointSurfaceCommand = new G4UIcmdWithAString("/pga/point/setSurface", this);
	_pointSurfaceCommand->SetGuidance("Set the volume name");
	_pointSurfaceCommand->SetGuidance("[usage] /pga/point/setSurface fileName");

	_pointCylinderCommand = new G4UIcommand("/pga/point/setCylinder", this);
	_pointCylinderCommand->SetGuidance("Set the cylinder size where the particles generated");

	auto posX = new G4UIparameter("posX", 'd', false);
	posX->SetGuidance("X coordinate of the cylinder center");

	auto posY = new G4UIparameter("posY", 'd', false);
	posY->SetGuidance("Y coordinate of the cylinder center");

	auto posZ = new G4UIparameter("posZ", 'd', false);
	posZ->SetGuidance("Z coordinate of the cylinder center");

	auto height = new G4UIparameter("h", 'd', false);
	height->SetGuidance("Cylinder height (in mm)");
	height->SetParameterRange("h >= 0");

	auto radiusMax = new G4UIparameter("rMax", 'd', false);
	radiusMax->SetGuidance("Cylinder outer radius (in mm)");
	radiusMax->SetParameterRange("rMax >= 0");

	auto radiusMin = new G4UIparameter("rMin", 'd', true);
	radiusMin->SetGuidance("Cylinder inner radius (in mm)");
	radiusMin->SetDefaultValue(0);
	radiusMin->SetParameterRange("rMin >= 0");

	auto thetaMin = new G4UIparameter("thetaMin", 'd', true);
	thetaMin->SetGuidance("Minimal angle (in degree)");
	thetaMin->SetDefaultValue(0);

	auto thetaMax = new G4UIparameter("thetaMax", 'd', true);
	thetaMax->SetGuidance("Maximum angle (in degree)");
	thetaMax->SetDefaultValue(360);

	auto part = new G4UIparameter("part", 's', true);
	part->SetGuidance("Where the points are generated");
	part->SetGuidance("INSIDE - inside the cylinder volume");
	part->SetGuidance("SURFACE_SIDE - on he outer side of the cylinder");
	part->SetGuidance("SURFACE_TOTAL - on the cylinder surface (side inner, side outer, top and bottom)");
	part->SetDefaultValue("SURFACE_SIDE");
	part->SetParameterCandidates("INSIDE SURFACE_SIDE SURFACE_TOTAL");

	_pointCylinderCommand->SetParameter(posX);
	_pointCylinderCommand->SetParameter(posY);
	_pointCylinderCommand->SetParameter(posZ);
	_pointCylinderCommand->SetParameter(height);
	_pointCylinderCommand->SetParameter(radiusMax);
	_pointCylinderCommand->SetParameter(radiusMin);
	// _pointCylinderCommand->SetParameter(thetaMin);
	// _pointCylinderCommand->SetParameter(thetaMax);
	_pointCylinderCommand->SetParameter(part);

	_pointMaterialInCylinderCommand = new G4UIcommand("/pga/point/setMaterialInCylinder", this);
	_pointMaterialInCylinderCommand->SetGuidance("Set the material in cylinder size where the particles are generated");

	auto posXMaterialInCylinder = new G4UIparameter("posX", 'd', false);
	posXMaterialInCylinder->SetGuidance("X coordinate of the cylinder center");

	auto posYMaterialInCylinder = new G4UIparameter("posY", 'd', false);
	posYMaterialInCylinder->SetGuidance("Y coordinate of the cylinder center");

	auto posZMaterialInCylinder = new G4UIparameter("posZ", 'd', false);
	posZMaterialInCylinder->SetGuidance("Z coordinate of the cylinder center");

	auto heightMax = new G4UIparameter("hMax", 'd', false);
	heightMax->SetGuidance("Cylinder max height (in mm)");
	heightMax->SetParameterRange("hMax >= 0");

	auto radiusMaxMaterialInCylinder = new G4UIparameter("rMax", 'd', false);
	radiusMaxMaterialInCylinder->SetGuidance("Cylinder outer radius (in mm)");
	radiusMaxMaterialInCylinder->SetParameterRange("rMax >= 0");

	auto thetaMinMaterialInCylinder = new G4UIparameter("thetaMin", 'd', true);
	thetaMinMaterialInCylinder->SetGuidance("Minimal angle (in degree)");
	thetaMinMaterialInCylinder->SetDefaultValue(0);

	auto thetaMaxMaterialInCylinder = new G4UIparameter("thetaMax", 'd', true);
	thetaMaxMaterialInCylinder->SetGuidance("Maximum angle (in degree)");
	thetaMaxMaterialInCylinder->SetDefaultValue(360);

	auto materialInCylinder = new G4UIparameter("material", 's', true);
	materialInCylinder->SetDefaultValue("");

	_pointMaterialInCylinderCommand->SetParameter(posXMaterialInCylinder);
	_pointMaterialInCylinderCommand->SetParameter(posYMaterialInCylinder);
	_pointMaterialInCylinderCommand->SetParameter(posZMaterialInCylinder);
	_pointMaterialInCylinderCommand->SetParameter(heightMax);
	_pointMaterialInCylinderCommand->SetParameter(radiusMaxMaterialInCylinder);
	_pointMaterialInCylinderCommand->SetParameter(materialInCylinder);
}

void PrimaryGeneratorActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
	if (command == _sourceTableCommand)
	{
		G4Tokenizer next(newValue);
        auto particleName = next();
        double particleEnergy = StoD(next());
		_primaryGeneratorAction->SetSource(new PgaSourceTable(particleName, particleEnergy*keV));
	}

	else if (command == _sourceTableFlatCommand)
	{
		G4Tokenizer next(newValue);
        auto particleName = next();
        double particleMinEnergy = StoD(next());
        double particleMaxEnergy = StoD(next());
		_primaryGeneratorAction->SetSource(new PgaSourceTableFlat(particleName, particleMinEnergy*keV, particleMaxEnergy*keV));
	}

	else if (command == _sourceDecay0Command)
	{
		G4Tokenizer next(newValue);
		auto maGeDecay0Interface = new MaGeDecay0Interface(next(), StoI(next()));
		_primaryGeneratorAction->SetParticleGun(maGeDecay0Interface);
		_primaryGeneratorAction->SetSource(new PgaSourceDecay0(newValue));
	}

#if HAS_BXDECAY0
	else if (command == _sourceBxDecay0Command)
	{
		G4Tokenizer next(newValue);
        auto nuclideName = next();
		auto bxDecay0ParticleGun = new BxDecay0ParticleGun(nuclideName);
		_primaryGeneratorAction->SetParticleGun(bxDecay0ParticleGun);
		_primaryGeneratorAction->SetSource(new PgaSourceBxDecay0(nuclideName));
	}
#endif

	else if (command == _sourceIonCommand)
	{
		G4Tokenizer next(newValue);
        auto ionA = StoI(next());
        auto ionZ = StoI(next());
        double ionE = StoD(next());
        auto ionJ = StoI(next());
		_primaryGeneratorAction->SetSource(new PgaSourceIon(ionA, ionZ, ionE*keV, ionJ));
	}

	else if (command == _pointFileCommand)
		_primaryGeneratorAction->SetPoint(new PgaPointFile(newValue));

	else if (command == _pointMaterialCommand)
		_primaryGeneratorAction->SetPoint(new PgaPointMaterial(newValue));

	else if (command == _pointVolumeCommand)
		_primaryGeneratorAction->SetPoint(new PgaPointVolume(newValue));

	else if (command == _pointSurfaceCommand)
		_primaryGeneratorAction->SetPoint(new PgaPointSurface(newValue));

	else if (command == _pointSingleCommand)
	{	
		G4Tokenizer next(newValue);
        auto x = StoD(next());
        auto y = StoD(next());
        auto z = StoD(next());
		_primaryGeneratorAction->SetPoint(new PgaPointSingle(G4ThreeVector(x, y, z)));
	}

	else if (command == _pointCylinderCommand)
	{	
		Cylinder cylinder;
		ICylinderPart* cylinderPart;
		 
		G4Tokenizer next(newValue);
        cylinder.center.setX(StoD(next()));
        cylinder.center.setY(StoD(next()));
        cylinder.center.setZ(StoD(next()));
        cylinder.height = StoD(next()) * mm;
        cylinder.radiusMax = StoD(next()) * mm;
        cylinder.radiusMin = StoD(next()) * mm;
		cylinder.angleMin = 0 * degree;
        cylinder.angleMax = 360 * degree;
        // cylinder.angleMin = StoD(next()) * degree;
        // cylinder.angleMax = StoD(next()) * degree;
        auto part = next();

		if (part == "INSIDE") cylinderPart = new PartInside(cylinder);
		else if (part == "SURFACE_SIDE") cylinderPart = new PartSideSurface(cylinder);
		else if (part == "SURFACE_TOTAL") cylinderPart = new PartTotalSurface(cylinder);
		else cylinderPart = new PartSideSurface(cylinder);

		_primaryGeneratorAction->SetPoint(new PgaPointCylinder(cylinderPart));
	}

	else if (command == _pointMaterialInCylinderCommand)
	{	
		MaterialInCylinderParameters cylinder;
		G4Tokenizer next(newValue);
        cylinder.center.setX(StoD(next()));
        cylinder.center.setY(StoD(next()));
        cylinder.center.setZ(StoD(next()));
        cylinder.heightMax = StoD(next()) * mm;
        cylinder.radiusMax = StoD(next()) * mm;
		cylinder.angleMin = 0 * degree;
        cylinder.angleMax = 360 * degree;
        cylinder.materialName = next();

		_primaryGeneratorAction->SetPoint(new PgaPointMaterialInCylinder(cylinder));
	}
}

void PrimaryGeneratorActionMessenger::SetVolumeCandidates(G4String listVolumes)
{
	_pointVolumeCommand->SetCandidates(listVolumes);
	_pointSurfaceCommand->SetCandidates(listVolumes);
}

void PrimaryGeneratorActionMessenger::SetMaterialCandidates(G4String listMaterials)
{
	_pointMaterialCommand->SetCandidates(listMaterials);
}