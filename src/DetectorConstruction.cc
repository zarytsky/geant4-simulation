#include "DetectorConstruction.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"

DetectorConstruction::DetectorConstruction(): G4VUserDetectorConstruction() 
{
	_messenger = new DetectorConstructionMessenger(this);
	_sdManager = G4SDManager::G4SDManager::GetSDMpointer();
	_logVolueStore = G4LogicalVolumeStore::GetInstance();
	_physVolumeStore = G4PhysicalVolumeStore::GetInstance();
	_gdmlParser = new G4GDMLParser();
}

DetectorConstruction::~DetectorConstruction() 
{
	delete _messenger;
}

G4VPhysicalVolume* DetectorConstruction::Construct() 
{  
	// _gdmlParser->Read("geometry/main.gdml");
	auto worldVolume = _gdmlParser->GetWorldVolume();
	if (worldVolume == 0) 
		G4Exception("DetectorConstruction::Construct()", "", FatalException, "World volume is null");
	worldVolume->GetLogicalVolume()->SetVisAttributes(G4VisAttributes::Invisible);
	return worldVolume;
}

void DetectorConstruction::ConstructSDandField()
{
	_messenger->SetLogVolNames(GetLogVolNames());
	_messenger->SetMaterialNames(GetMaterialNames());
	
	auto pga = (PrimaryGeneratorAction*) G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction();
	pga->SetMessengerMaterials(GetMaterialNames());
	pga->SetMessengerVolumes(GetPhysVolNames());

	auto steppingAction = (SteppingAction*) G4RunManager::GetRunManager()->GetUserSteppingAction();
	steppingAction->SetMessengerVolumes(GetPhysVolNames());
}

void DetectorConstruction::SetDetectorByVolume(G4LogicalVolume* logVolume, SdStoreData* sdData, G4int ntupleGroupId)
{
	auto sensitiveDetector = new SensitiveDetector(logVolume->GetName(), sdData, ntupleGroupId);
	logVolume->SetSensitiveDetector(sensitiveDetector);
	_sdManager->AddNewDetector(sensitiveDetector);

	auto runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
    runAction->AddDataToStore(sensitiveDetector);
}


void DetectorConstruction::SetDetectorByName(G4String volumeName, SdStoreData* sdData, G4int ntupleGroupId)
{	
	SetDetectorByVolume(_logVolueStore->GetVolume(volumeName), sdData, ntupleGroupId);
}

void DetectorConstruction::SetDetectorsByMaterial(G4String materialName, SdStoreData* sdData, G4int ntupleGroupId)
{
	for (auto &physVolume: *_physVolumeStore)
	{
		auto logVolume = physVolume->GetLogicalVolume();
		if (logVolume->GetMaterial()->GetName() == materialName)
			SetDetectorByVolume(logVolume, sdData, ntupleGroupId);
	}
}

void DetectorConstruction::SetVolumeColor(G4String volumeName, G4Color color)
{
	_logVolueStore->GetVolume(volumeName)->SetVisAttributes(new G4VisAttributes(true, color));
}

void DetectorConstruction::SetVolumesColorByMaterial(G4String materialName, G4Color color)
{
	for (auto &logVolume: *_logVolueStore)
	{
		if (logVolume->GetMaterial()->GetName() == materialName)
			logVolume->SetVisAttributes(new G4VisAttributes(true, color));
	}
}

G4String DetectorConstruction::GetLogVolNames()
{
	G4String totalNames = "";
	for (auto &logVolume: *_logVolueStore)
		totalNames += logVolume->GetName() + " ";
	return totalNames;
}

G4String DetectorConstruction::GetPhysVolNames()
{
	G4String totalNames = "";
	for (auto &physVolume: *_physVolumeStore)
		totalNames += physVolume->GetName() + " ";
	return totalNames;
}

G4String DetectorConstruction::GetMaterialNames()
{
	G4String totalNames = "";
	std::vector<G4String> materialNames;
	for (auto &volume: *_logVolueStore)
	{
		auto materialName = volume->GetMaterial()->GetName();
		if (!totalNames.contains(materialName))
			totalNames += materialName + " ";
	}
	return totalNames;
}

G4double DetectorConstruction::GetMass(G4String logVolumeName)
{
	auto mass = _logVolueStore->GetVolume(logVolumeName)->GetMass();
	return mass;
}

G4double DetectorConstruction::GetVolume(G4String logVolumeName)
{
	auto volume = _logVolueStore->GetVolume(logVolumeName)->GetSolid()->GetCubicVolume();
	return volume;
}

G4double DetectorConstruction::GetSurface(G4String logVolumeName)
{
	auto surface = _logVolueStore->GetVolume(logVolumeName)->GetSolid()->GetSurfaceArea();
	return surface;
}





