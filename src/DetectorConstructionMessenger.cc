#include "DetectorConstructionMessenger.hh"

DetectorConstructionMessenger::DetectorConstructionMessenger(DetectorConstruction* detectorConstruction)
    : _detectorConstruction(detectorConstruction) 
{ 
    InitEdepParticle();
    InitEdepParticleMaterial();
    InitEdepStandard();
    InitEdepStandardMaterial();
    InitEdepTotal();
    InitEdepTotalMaterial();
    InitGetProperties();
    InitVolumeColor();
    InitVolumeColorMaterial();
    InitEdepProcess();
    
    _volumesList = new G4UIcmdWithoutParameter("/detector/volumesList", this);
    _volumesList->SetGuidance("Get the list of logical volumes available");

    _uiManager = G4UImanager::GetUIpointer();
}

void DetectorConstructionMessenger::InitGetProperties()
{
    _getMass = new G4UIcmdWithAString("/geometry/getMass", this);
    _getMass->SetGuidance("Get the mass of the volume");
    _getMass->SetParameterName("logVolume", false);

    _getVolume = new G4UIcmdWithAString("/geometry/getVolume", this);
    _getVolume->SetGuidance("Get the cubic volume of the volume");
    _getVolume->SetParameterName("logVolume", false);

    _getSurface = new G4UIcmdWithAString("/geometry/getSurface", this);
    _getSurface->SetGuidance("Get the surface area of the volume");
    _getSurface->SetParameterName("logVolume", false);
}

void DetectorConstructionMessenger::InitEdepParticle()
{
    _setEdepParticle = new G4UIcommand("/detector/setEdepParticle", this);
    _setEdepParticle->SetGuidance("Set the particle name energy deposit of");
    _setEdepParticle->SetGuidance("which will be stored");

    auto parameterLogVolume = new G4UIparameter("logVolumeName", 's', false);
    parameterLogVolume->SetGuidance("The logical volume name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    auto parameterParticleName = new G4UIparameter("particleName", 's', false);
    parameterParticleName->SetGuidance("The particle name ");
    parameterParticleName->SetParameterCandidates(GetParticleNames());

    _setEdepParticle->SetParameter(parameterLogVolume);
    _setEdepParticle->SetParameter(parameterNtupleGroupId);
    _setEdepParticle->SetParameter(parameterParticleName);
}

void DetectorConstructionMessenger::InitEdepStandard()
{
    _setEdepStandard = new G4UIcommand("/detector/setEdepStandard", this);
    _setEdepStandard->SetGuidance("Store the energy deposit of Gamma/Beta and Alpha particles");

    auto parameterLogVolume = new G4UIparameter("logVolumeName", 's', false);
    parameterLogVolume->SetGuidance("The logical volume name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    _setEdepStandard->SetParameter(parameterLogVolume);
    _setEdepStandard->SetParameter(parameterNtupleGroupId);
}

void DetectorConstructionMessenger::InitEdepTotal()
{
    _setEdepTotal = new G4UIcommand("/detector/setEdepTotal", this);
    _setEdepTotal->SetGuidance("Store the total energy deposit");

    auto parameterLogVolume = new G4UIparameter("logVolumeName", 's', false);
    parameterLogVolume->SetGuidance("The logical volume name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    _setEdepTotal->SetParameter(parameterLogVolume);
    _setEdepTotal->SetParameter(parameterNtupleGroupId);
}

void DetectorConstructionMessenger::InitEdepParticleMaterial()
{
    _setEdepParticleMaterial = new G4UIcommand("/detector/setEdepParticleByMaterial", this);
    _setEdepParticleMaterial->SetGuidance("Set the particle name energy deposit of");
    _setEdepParticleMaterial->SetGuidance("which will be stored");

    auto parameterMaterial = new G4UIparameter("materialName", 's', false);
    parameterMaterial->SetGuidance("The material name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    auto parameterParticleName = new G4UIparameter("particleName", 's', false);
    parameterParticleName->SetGuidance("The particle name ");
    parameterParticleName->SetParameterCandidates(GetParticleNames());

    _setEdepParticleMaterial->SetParameter(parameterMaterial);
    _setEdepParticleMaterial->SetParameter(parameterNtupleGroupId);
    _setEdepParticleMaterial->SetParameter(parameterParticleName);
}

void DetectorConstructionMessenger::InitEdepStandardMaterial()
{
    _setEdepStandardMaterial = new G4UIcommand("/detector/setEdepStandardByMaterial", this);
    _setEdepStandardMaterial->SetGuidance("Store the energy deposit of Gamma/Beta and Alpha particles");

    auto parameterMaterial = new G4UIparameter("materialName", 's', false);
    parameterMaterial->SetGuidance("The material name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    _setEdepStandardMaterial->SetParameter(parameterMaterial);
    _setEdepStandardMaterial->SetParameter(parameterNtupleGroupId);
}

void DetectorConstructionMessenger::InitEdepTotalMaterial()
{
    _setEdepTotalMaterial = new G4UIcommand("/detector/setEdepTotalByMaterial", this);
    _setEdepTotalMaterial->SetGuidance("Store the total energy deposit for volumes with material");

    auto parameterMaterial = new G4UIparameter("materialName", 's', false);
    parameterMaterial->SetGuidance("The material name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    _setEdepTotalMaterial->SetParameter(parameterMaterial);
    _setEdepTotalMaterial->SetParameter(parameterNtupleGroupId);
}

void DetectorConstructionMessenger::InitVolumeColor()
{
    _setVolumeColor = new G4UIcommand("/geometry/setVolumeColor", this);
    _setVolumeColor->SetGuidance("To set the volume color (r, g, b, a)");

    auto parameterVolumeName = new G4UIparameter("volumeName", 's', false);
    parameterVolumeName->SetGuidance("Set the logical volume name");

    auto parameterColorR = new G4UIparameter("colorR", 'd', false);
    parameterColorR->SetGuidance("Set the color's red");
    parameterColorR->SetDefaultValue(1);
    parameterColorR->SetParameterRange("colorR >= 0 && colorR <= 1");

    auto parameterColorG = new G4UIparameter("colorG", 'd', false);
    parameterColorG->SetGuidance("Set the color's green");
    parameterColorG->SetDefaultValue(1);
    parameterColorG->SetParameterRange("colorG >= 0 && colorG <= 1");

    auto parameterColorB = new G4UIparameter("colorB", 'd', false);
    parameterColorB->SetGuidance("Set the color's blue");
    parameterColorB->SetDefaultValue(1);
    parameterColorB->SetParameterRange("colorB >= 0 && colorB <= 1");

    auto parameterColorA = new G4UIparameter("colorA", 'd', true);
    parameterColorA->SetGuidance("Set the color's alpha (transparency)");
    parameterColorA->SetDefaultValue(1);
    parameterColorA->SetParameterRange("colorA >= 0 && colorA <= 1");

    _setVolumeColor->SetParameter(parameterVolumeName);
    _setVolumeColor->SetParameter(parameterColorR);
    _setVolumeColor->SetParameter(parameterColorG);
    _setVolumeColor->SetParameter(parameterColorB);
    _setVolumeColor->SetParameter(parameterColorA);
}

void DetectorConstructionMessenger::InitVolumeColorMaterial()
{
    _setVolumeColorMaterial = new G4UIcommand("/geometry/setVolumeColorByMaterial", this);
    _setVolumeColorMaterial->SetGuidance("To set the volumes color (r, g, b, a) with the certain material");

    auto parameterMaterial = new G4UIparameter("materialName", 's', false);
    parameterMaterial->SetGuidance("Set the material name");

    auto parameterColorR = new G4UIparameter("colorR", 'd', false);
    parameterColorR->SetGuidance("Set the color's red");
    parameterColorR->SetDefaultValue(1);
    parameterColorR->SetParameterRange("colorR >= 0 && colorR <= 1");

    auto parameterColorG = new G4UIparameter("colorG", 'd', false);
    parameterColorG->SetGuidance("Set the color's green");
    parameterColorG->SetDefaultValue(1);
    parameterColorG->SetParameterRange("colorG >= 0 && colorG <= 1");

    auto parameterColorB = new G4UIparameter("colorB", 'd', false);
    parameterColorB->SetGuidance("Set the color's blue");
    parameterColorB->SetDefaultValue(1);
    parameterColorB->SetParameterRange("colorB >= 0 && colorB <= 1");

    auto parameterColorA = new G4UIparameter("colorA", 'd', true);
    parameterColorA->SetGuidance("Set the color's alpha (transparency)");
    parameterColorA->SetDefaultValue(1);
    parameterColorA->SetParameterRange("colorA >= 0 && colorA <= 1");

    _setVolumeColorMaterial->SetParameter(parameterMaterial);
    _setVolumeColorMaterial->SetParameter(parameterColorR);
    _setVolumeColorMaterial->SetParameter(parameterColorG);
    _setVolumeColorMaterial->SetParameter(parameterColorB);
    _setVolumeColorMaterial->SetParameter(parameterColorA);
}

void DetectorConstructionMessenger::InitEdepProcess()
{
    _setEdepProcess = new G4UIcommand("/detector/setEdepProcess", this);
    _setEdepProcess->SetGuidance("Set the physics process energy deposit of");
    _setEdepProcess->SetGuidance("which will be stored");

    auto parameterLogVolume = new G4UIparameter("logVolumeName", 's', false);
    parameterLogVolume->SetGuidance("The logical volume name");

    auto parameterProcess = new G4UIparameter("processName", 's', false);
    parameterProcess->SetGuidance("The physics process name");

    auto parameterNtupleGroupId = new G4UIparameter("ntupleGroupId", 'i', true);
    parameterNtupleGroupId->SetGuidance("The Ntuple Group ID");
    parameterNtupleGroupId->SetDefaultValue(0);
    parameterNtupleGroupId->SetParameterRange("ntupleGroupId >= 0");

    _setEdepProcess->SetParameter(parameterLogVolume);
    _setEdepProcess->SetParameter(parameterProcess);
    _setEdepProcess->SetParameter(parameterNtupleGroupId);
}

G4String DetectorConstructionMessenger::GetProcessNames()
{
    auto particleTable = G4ParticleTable::GetParticleTable();
    G4String candidateList;
	for(G4int i = 0; i < particleTable->entries(); i++)
    {
        auto processManager = new G4ProcessManager(particleTable->GetParticle(i));
        auto processVector = processManager->GetProcessList();
        for (auto j = 0; j < processVector->entries(); j++)
            candidateList += (*processVector)[j]->GetProcessName() + " ";
    }
    return candidateList;
}

G4String DetectorConstructionMessenger::GetParticleNames()
{
	auto particleTable = G4ParticleTable::GetParticleTable();
    G4String candidateList;
	for(G4int i = 0; i < particleTable->entries(); i++)
		candidateList += particleTable->GetParticleName(i) + " ";
	return candidateList;
}

DetectorConstructionMessenger::~DetectorConstructionMessenger()
{
	delete _setEdepParticle;  
    delete _volumesList;
    delete _setEdepStandard;
}

void DetectorConstructionMessenger::SetLogVolNames(G4String names)
{
    _setEdepParticle->GetParameter(0)->SetParameterCandidates(names);
    _setEdepStandard->GetParameter(0)->SetParameterCandidates(names);
    _setEdepTotal->GetParameter(0)->SetParameterCandidates(names);
    _setEdepProcess->GetParameter(0)->SetParameterCandidates(names);

    _setVolumeColor->GetParameter(0)->SetParameterCandidates(names);
    _getMass->SetCandidates(names);
    _getVolume->SetCandidates(names);
    _getSurface->SetCandidates(names);

    _setEdepProcess->GetParameter(1)->SetParameterCandidates(GetProcessNames());
}

void DetectorConstructionMessenger::SetMaterialNames(G4String names)
{
    _setEdepParticleMaterial->GetParameter(0)->SetParameterCandidates(names);
    _setEdepStandardMaterial->GetParameter(0)->SetParameterCandidates(names);
    _setEdepTotalMaterial->GetParameter(0)->SetParameterCandidates(names);
    _setVolumeColorMaterial->GetParameter(0)->SetParameterCandidates(names);
}

void DetectorConstructionMessenger::SetNewValue(G4UIcommand* command, G4String newValues)
{
    if (command == _setEdepParticle)
    {
        G4Tokenizer next(newValues);
        auto logVolName = next();
        auto ntupleGroupId = StoI(next());
        auto particleName = next();
        
        _detectorConstruction->SetDetectorByName(logVolName, 
            new SdEnergyDeposit(particleName), ntupleGroupId);
    }

    else if (command == _setEdepParticleMaterial)
    {
        G4Tokenizer next(newValues);
        auto materialName = next();
        auto ntupleGroupId = StoI(next());
        auto particleName = next();
        
        for (auto &logVolume: *G4LogicalVolumeStore::GetInstance())
        {
            if (logVolume->GetMaterial()->GetName() == materialName)
            {
                _detectorConstruction->SetDetectorByName(logVolume->GetName(), 
                    new SdEnergyDeposit(particleName), ntupleGroupId);
            }
        }

        // _detectorConstruction->SetDetectorsByMaterial(materialName, 
        //     new SdEnergyDeposit(particleName), ntupleGroupId);
    }

    else if (command == _setEdepStandard)
    {
        G4Tokenizer next(newValues);
        auto logVolName = next();
        auto ntupleGroupId = StoI(next());
        
        _detectorConstruction->SetDetectorByName(logVolName, 
            new SdEdepStandard, ntupleGroupId);
    }

    else if (command == _setEdepStandardMaterial)
    {
        G4Tokenizer next(newValues);
        auto materialName = next();
        auto ntupleGroupId = StoI(next());
        
        for (auto &physVolume: *G4PhysicalVolumeStore::GetInstance())
        {
            auto logVolume = physVolume->GetLogicalVolume();
            if (logVolume->GetMaterial()->GetName() == materialName)
            {
                _detectorConstruction->SetDetectorByName(logVolume->GetName(), 
                    new SdEdepStandard, ntupleGroupId);
            }
        }
    }

    else if (command == _setEdepTotal)
    {
        G4Tokenizer next(newValues);
        auto logVolName = next();
        auto ntupleGroupId = StoI(next());
        
        _detectorConstruction->SetDetectorByName(logVolName, 
            new SdEdepTotal, ntupleGroupId);
    }

    else if (command == _setEdepTotalMaterial)
    {
        G4Tokenizer next(newValues);
        auto materialName = next();
        auto ntupleGroupId = StoI(next());
        
        for (auto &physVolume: *G4PhysicalVolumeStore::GetInstance())
        {
            auto logVolume = physVolume->GetLogicalVolume();
            if (logVolume->GetMaterial()->GetName() == materialName)
            {
                _detectorConstruction->SetDetectorByName(logVolume->GetName(), 
                    new SdEdepTotal, ntupleGroupId);
            }
        }
    }

    else if (command == _setEdepProcess)
    {
        G4Tokenizer next(newValues);
        auto logVolName = next();
        auto processName = next();
        auto ntupleGroupId = StoI(next());
        
        _detectorConstruction->SetDetectorByName(logVolName, 
            new SdEdepProcess(processName), ntupleGroupId);
    }

    else if (command == _setVolumeColor)
    {
        G4Tokenizer next(newValues);
        auto logVolName = next();
        auto colorR = StoD(next());
        auto colorG = StoD(next());
        auto colorB = StoD(next());
        auto colorA = StoD(next());
        
        _detectorConstruction->SetVolumeColor(logVolName, 
            G4Color(colorR, colorG, colorB, colorA));

        _uiManager->ApplyCommand("/vis/scene/notifyHandlers");
    }

    else if (command == _setVolumeColorMaterial)
    {
        G4Tokenizer next(newValues);
        auto materialName = next();
        auto colorR = StoD(next());
        auto colorG = StoD(next());
        auto colorB = StoD(next());
        auto colorA = StoD(next());
        
        _detectorConstruction->SetVolumesColorByMaterial(materialName, 
            G4Color(colorR, colorG, colorB, colorA));

        _uiManager->ApplyCommand("/vis/scene/notifyHandlers");        
    }

    else if (command == _volumesList)
        G4cout << _setEdepParticle->GetParameter(0)->GetParameterCandidates() << G4endl;

    else if (command == _getMass)
        G4cout << _detectorConstruction->GetMass(newValues)/kg << " kg" << G4endl;

    else if (command == _getVolume)
        G4cout << _detectorConstruction->GetVolume(newValues)/cm3 << " cm3" << G4endl;

    else if (command == _getSurface)
        G4cout << _detectorConstruction->GetSurface(newValues)/cm2 << " cm2" << G4endl;

}

